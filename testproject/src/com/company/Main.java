package com.company;

import java.util.function.IntFunction;

public class Main {

    interface Foo {
        int x = 10;
    }

    public static void main(String[] args) {
	// write your code here
        Foo.x = 20;

        System.out.println(Foo.x);
    }
}
