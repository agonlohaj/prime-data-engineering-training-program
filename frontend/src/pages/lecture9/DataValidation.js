/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import { connect } from 'react-redux';
import Code from "presentations/Code";
import DocumentationToDoImage from "assets/images/lecture11/documentation-todo.png";
import DocumentationDone from "assets/images/lecture11/documentation-done.png";
import ClassDocumentation from "assets/images/lecture11/class-documentation.png";
import CodingConventionMotivation from "assets/images/lecture11/coding_conventions_motivation.png";
import CodingWithStyle from "assets/images/lecture11/coding-with-style.png";
import GuardIfElse from "assets/images/lecture11/guard-if-else.png";
import WideHorizontalIfElse from "assets/images/lecture11/wide-horizontal-if-else.png";
import DragonBallWide from "assets/images/lecture11/dragon-ball-wide.png";
import JavaDocExample from "assets/images/lecture11/javadoc.png";
import { Bold, Italic } from "presentations/Label";
import SimpleLink from "presentations/rows/SimpleLink";
import Exercise from "presentations/Exercise";

const styles = ({ typography }) => ({
  root: {},
})

const sampleData = `id,name,age
20,"Filan","20"
25,"Fistek","25A"`

const inferringSchema = `spark.read().csv(myPath);

Output DDL: id STRING, name STRING, age STRING`

const specifyingSchema =`spark.read().schema(StructType schema = DataTypes.createStructType(new StructField[] {
        DataTypes.createStructField("id",  DataTypes.IntegerType, true),
        DataTypes.createStructField("name", DataTypes.StringType, true),
        DataTypes.createStructField("age", DataTypes.StringType, true),
}).csv(myPath);

Output DDL: id INT, name STRING, age STRING`

class DataValidation extends React.Component {

  render() {
    const { classes, section } = this.props
    const whyValidating = section.children[0]
    const validatingForConsistency = section.children[1]
    const formatStandards = section.children[2]
    const schemaValidation = section.children[3]
    const contentValidation = section.children[4]

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography >
          Data validation is an essential part of any data handling task whether you’re in the field collecting
          information, analyzing data, or preparing to present data to stakeholders. If data isn’t accurate from the
          start, your results definitely won’t be accurate either.
        </Typography>

        <Typography>
          That’s why it’s necessary to verify and validate data before it is used. While data validation is a critical
          step in any data workflow, it’s often skipped over. It may seem as if data validation is a step that slows
          down your pace of work, however, it is essential because it will help you create the best results possible.
        </Typography>

        <Typography>
          These days data validation can be a much quicker process than you might’ve thought. With data integration
          platforms that can incorporate and automate validation processes, validation can be treated as an essential
          ingredient to your workflow rather than an additional step.
        </Typography>

        <Typography variant={'title'} id={whyValidating.id}>
          {whyValidating.display}
        </Typography>

        <Typography>
          Validating the accuracy, clarity, and details of data is necessary to mitigate any project defects.
          Without validating data, you run the risk of basing decisions on data with imperfections that are not
          accurately representative of the situation at hand.
        </Typography>

        <Typography>
          While verifying data inputs and values is important, it is also necessary to validate the data model itself.
          If the data model is not structured or built correctly, you will run into issues when trying to use data
          files in various applications and software.
        </Typography>

        <Typography>
          Both the structure and content of data files will dictate what exactly you can do with data. Using validation
          rules to cleanse data before use helps to mitigate “garbage in = garbage out” scenarios. Ensuring the
          integrity of data helps to ensure the legitimacy of your conclusions.
        </Typography>

        <Typography variant={'title'} id={validatingForConsistency.id}>
          {validatingForConsistency.display}
        </Typography>

        <Typography>
          The most straightforward (and arguably the most essential) rules used in data validation are rules that ensure
          data integrity. You’re probably familiar with these types of practices. Spell check? Data validation.
          Minimum password length? Data validation.
        </Typography>

        <Typography>
          Every organization will have its own unique rules for how data should be stored and maintained. Setting basic
          data validation rules will help your company uphold organized standards that will effectively make working
          with data more efficient. Some other common examples of data validation rules that help maintain integrity
          and clarity include:

          <ol>
            <li><Bold>Data type</Bold>: ex. integer, float, string</li>
            <li><Bold>Range</Bold>: ex. a number between 35-40</li>
            <li><Bold>Uniqueness</Bold>: ex. postal code</li>
            <li><Bold>Consistent expressions</Bold>: ex. using one of St., Str, Street</li>
            <li><Bold>No null values</Bold>: ex. using one of St., Str, Street</li>
          </ol>
        </Typography>

        <Typography variant={'title'} id={formatStandards.id}>
          {formatStandards.display}
        </Typography>

        <Typography>
          Validating the structure of data is just as important as validating the data itself. Doing so will ensure
          that you are using the appropriate data model for the formats that are compatible with the applications you
          would like to use data in.
        </Typography>

        <Typography>
          File formats and their standards are maintained by non-profit organizations, government departments,
          industry advisory panels, and private companies. With their assistance, they help to continuously develop,
          document, and define file structures that hold data.
        </Typography>

        <Typography>
          When validating data, the standards and structure of the data model that the dataset is stored in should be
          well understood. Failing to do so may result in files that are incompatible with applications and other
          datasets with which you may want to integrate that data.
        </Typography>

        <Typography variant={'title'} id={schemaValidation.id}>
          {schemaValidation.display}
        </Typography>

        <Typography>
          Consider that we have a CSV file that contains: <Code>{sampleData}</Code>
        </Typography>

        <Typography>
          There are two ways to read this file through Spark:

          <ul>
            <li><Bold>Inferring schema</Bold>: Spark dynamically creates a schema that it think is best</li>
            <li><Bold>Specifying schema</Bold>: we set the list of columns and types that we want to read.</li>
          </ul>
        </Typography>

        <Typography>
          To infer the schema, we can use: <Code>{inferringSchema}</Code>
        </Typography>

        <Typography>We can see that every field is read as string (not necessarily - Spark decides for this one). We might want, however, to read the
        ID as integer so we can use analytic functions that apply to numbers.
        </Typography>

        <Typography>
          To do that, we can specify a schema while reading the file: <Code>{specifyingSchema}</Code>
        </Typography>

        <Typography>We have to be careful when it comes to deciding about this setting:

          <ul>
            <li>When inferring the schema, Spark gets a sample of multiple rows and decides what type to assign to each column.
              This results in two jobs being created, one to read the data and the other to understand the schema. This might
              be costly if the dataset is huge.
            </li>
            <li>In the other hand, when specifying the schema, Spark uses our implied schema, therefore it only runs one job,
              and that is data reading. However, if we specify an Integer for example to '20A' for example, we will get a
              null.
            </li>
          </ul>
        </Typography>

        <Typography>
          Why is this a problem? Because when working with clients, you are not guaranteed that you will get
          consistent data every batch, as they upgrade their systems and do changes. That is why it is up to the
          developer to decide whether to infer schema or specify one (but with the cost of miss-converting values).
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(DataValidation)
