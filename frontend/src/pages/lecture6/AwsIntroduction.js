/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import AwsBigDataPlatformImg from 'assets/images/lecture6/aws_big_data_platform.jpeg'

const styles = ({ typography }) => ({
  root: {},
})


class AwsIntroduction extends React.Component {
  render() {
    const { classes, section } = this.props
    const whatIsServerless = section.children[0]
    const whyServerless = section.children[1]

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography>
          Amazon Web Services (AWS) provides a broad platform of managed services to help you build, secure, and
          seamlessly scale end-to-end big data applications quickly and with ease. Whether your applications require
          real-time streaming or batch data processing, AWS provides the infrastructure and tools to tackle your
          next big data project. No hardware to procure, no infrastructure to maintain and scale—only what you need
          to collect, store, process, and analyze big data. AWS has an ecosystem of analytical solutions specifically
          designed to handle this growing amount of data and provide insight into your business.
        </Typography>

        <Typography>
          <img src={AwsBigDataPlatformImg} />
        </Typography>

        <Typography>
          The following services for collecting, processing, storing, and analyzing big data are described in order:
          <ul>
            <li>Amazon S3</li>
            <li>Amazon Redshift</li>
            <li>Amazon Elastic MapReduce (EMR)</li>
            <li>Amazon Glue</li>
            <li>Amazon Athena</li>
            <li>Amazon Kinesis</li>
          </ul>
        </Typography>

        <Typography id={whatIsServerless.id} variant={'title'}>
          {whatIsServerless.display}
        </Typography>

        <Typography>
          Serverless is the native architecture of the cloud that enables you to shift more of your operational
          responsibilities to AWS, increasing your agility and innovation. Serverless allows you to build and run
          applications and services without thinking about servers. It eliminates infrastructure management tasks
          such as server or cluster provisioning, patching, operating system maintenance, and capacity provisioning.
          You can build them for nearly any type of application or backend service, and everything required to run
          and scale your application with high availability is handled for you.
        </Typography>

        <Typography id={whyServerless.id} variant={'title'}>
          {whyServerless.display}
        </Typography>

        <Typography>
          Serverless enables you to build modern applications with increased agility and lower total cost of ownership.
          Building serverless applications means that your developers can focus on their core product instead of
          worrying about managing and operating servers or runtimes, either in the cloud or on-premises. This reduced
          overhead lets developers reclaim time and energy that can be spent on developing great products which scale
          and that are reliable.
        </Typography>



      </Fragment>
    )
  }
}

export default withStyles(styles)(AwsIntroduction)
