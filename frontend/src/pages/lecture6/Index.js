/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import React from "react";
import Intro from "pages/lecture6/Intro";
import AwsIntroduction from "pages/lecture6/AwsIntroduction";
import AwsS3 from "pages/lecture6/AwsS3";
import AwsGlue from "pages/lecture6/AwsGlue";
import AwsAthena from "pages/lecture6/AwsAthena";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_6.AWS_BIG_DATA_SERVICES_INTRO:
        return <AwsIntroduction {...props} />
      case PAGES.LECTURE_6.AWS_S3:
        return <AwsS3 {...props} />
      case PAGES.LECTURE_6.AWS_GLUE:
        return <AwsGlue {...props} />
      case PAGES.LECTURE_6.AWS_ATHENA:
        return <AwsAthena {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
