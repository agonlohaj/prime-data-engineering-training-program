/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import AwsGlueImg from 'assets/images/lecture6/aws_glue.png'
import GlueArchitectureImg from 'assets/images/lecture6/glue_architecture.png'

const styles = ({ typography }) => ({
  root: {},
})


class AwsGlue extends React.Component {
  render() {
    const { classes, section } = this.props
    const idealUsagePatterns = section.children[0]
    const performance = section.children[1]
    const scalabilityAndElasticity = section.children[2]
    const referenceArchitecture = section.children[3]

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography>
          <img src={AwsGlueImg} />
        </Typography>

        <Typography>
          AWS Glue is a fully managed extract, transform, and load (ETL) service that you can use to catalog your data,
          clean it, enrich it, and move it reliably between data stores. With AWS Glue, you can significantly reduce
          the cost, complexity, and time spent creating ETL jobs.
        </Typography>

        <Typography>
          AWS Glue is Serverless, so there is no infrastructure to setup or manage. You pay only for the resources
          consumed while your jobs are running.
        </Typography>

        <Typography>
          <ul>
            AWS Glue simplifies many tasks when you are building a data warehouse:
            <li>
              Discovers and catalogs metadata about your data stores into a central catalog. You can process
              semi-structured data, such as clickstream or process logs.
            </li>
            <li>
              Populates the AWS Glue Data Catalog with table definitions from scheduled crawler programs. Crawlers
              call classifier logic to infer the schema, format, and data types of your data. This metadata is stored
              as tables in the AWS Glue Data Catalog and used in the authoring process of your ETL jobs.
            </li>
            <li>
              Generates ETL scripts to transform, flatten, and enrich your data from source to target.
            </li>
            <li>
              Detects schema changes and adapts based on your preferences.
            </li>
            <li>
              Triggers your ETL jobs based on a schedule or event. You can initiate jobs automatically to move your
              data into your data warehouse. Triggers can be used to create a dependency flow between jobs.
            </li>
            <li>
              Gathers runtime metrics to monitor the activities of your data warehouse.
            </li>
            <li>
              Handles errors and retries automatically.
            </li>
            <li>
              Scales resources, as needed, to run your jobs.
            </li>
          </ul>
        </Typography>

        <Typography id={idealUsagePatterns.id} variant={'title'}>
          {idealUsagePatterns.display}
        </Typography>

        <Typography>
          AWS Glue is designed to easily prepare data for extract, transform, and load (ETL) jobs. Using AWS Glue
          gives you the following benefits:
          <ul>
            <li>
              AWS Glue can automatically crawl your data and generate code to execute or data transformations and
              loading processes.
            </li>
            <li>
              Integration with services like Amazon Athena, Amazon EMR, and Amazon Redshift
            </li>
            <li>
              Serverless, no infrastructure to provision or manage
            </li>
            <li>
              AWS Glue generates ETL code that is customizable, reusable, and portable, using familiar technology –
              Python and Spark.
            </li>
          </ul>
        </Typography>

        <Typography id={performance.id} variant={'title'}>
          {performance.display}
        </Typography>

        <Typography>
          AWS Glue uses a scale-out Apache Spark environment to load your data into its destination. You can simply
          specify the number of Data Processing Units (DPUs) that you want to allocate to your ETL job. An AWS Glue
          ETL job requires a minimum of 2 DPUs. By default, AWS Glue allocates 10 DPUs to each ETL job. Additional
          DPUs can be added to increase the performance of your ETL job. Multiple jobs can be triggered in parallel
          or sequentially by triggering them on a job completion event. You can also trigger one or more AWS Glue jobs
          from an external source such as an AWS Lambda function.
        </Typography>

        <Typography id={scalabilityAndElasticity.id} variant={'title'}>
          {scalabilityAndElasticity.display}
        </Typography>

        <Typography>
          AWS Glue provides a managed ETL service that runs on a Serverless Apache Spark environment. This allows you
          to focus on your ETL job and not worry about configuring and managing the underlying compute resources. AWS
          Glue works on top of the Apache Spark environment to provide a scale-out execution environment for your data
          transformation jobs.
        </Typography>

        <Typography id={referenceArchitecture.id} variant={'title'}>
          {referenceArchitecture.display}
        </Typography>

        <Typography>
          <img src={GlueArchitectureImg} />
        </Typography>



      </Fragment>
    )
  }
}

export default withStyles(styles)(AwsGlue)
