/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import {Bold, Italic} from "presentations/Label";
import CloudComputingSolutions from 'assets/images/lecture5/cloud_computing_solutions.png'
import SimpleLink from "presentations/rows/SimpleLink";

const styles = ({ typography }) => ({
  root: {},
})


class CloudComputing extends React.Component {
  render() {
    const { classes, section } = this.props

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>
        <Typography variant={'p'}>
          Cloud computing is the on-demand delivery of compute power, database, storage, applications, and other IT
          resources via the internet with pay-as-you-go pricing.
        </Typography>

        <Typography className={'test'}>
            <Italic>
              The availability of high-capacity networks, low-cost computers and storage devices as well as the
              widespread adoption of hardware virtualization, service-oriented architecture and autonomic and
              utility computing has led to growth in cloud computing.
            </Italic> -- Wikipedia

        </Typography>

        <Typography>
          <img src={CloudComputingSolutions}/>
        </Typography>

        <Typography>
          "Cloud computing" was popularized with Amazon.com releasing its Elastic Compute Cloud product in 2006,
          references to the phrase "cloud computing" appeared as early as 1996, with the first known mention in
          a <SimpleLink href="https://www.technologyreview.com/2011/10/31/257406/who-coined-cloud-computing/">
          Compaq Internal Document</SimpleLink>.
        </Typography>

        <Typography>
          Main players in Cloud industry are:
          <ul>
            <li>Amazon Web Services</li>
            <li>Microsoft Azure</li>
            <li>GCP</li>
            <li>Alibaba</li>
            <li>Oracle</li>
            <li>IBM</li>
          </ul>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(CloudComputing)
