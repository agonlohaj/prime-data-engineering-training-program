/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import PageLink from "presentations/rows/nav/PageLink";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import Exercise from 'presentations/Exercise'
import Code from 'presentations/Code'
import { Bold } from 'presentations/Label'

const styles = ({ typography }) => ({
  root: {},
})

const sqlExample = `// Register the DataFrame as a SQL temporary view
df.createOrReplaceTempView("people");

// Run SQL query
Dataset<Row> sqlDF = spark.sql("SELECT * FROM people");
sqlDF.show();

// To create a Dataset from an ArrayList
List<Row> inMemory = new ArrayList<Row>();
// add elements as Row instances
inMemory.add(RowFactory.create(null, "11-1-2019"));
inMemory.add(RowFactory.create("FATAL", "11-1-2019"));
inMemory.add(RowFactory.create("FATAL", "11-1-2019"));
inMemory.add(RowFactory.create("WARN", "12-02-2019"));
inMemory.add(RowFactory.create("INFO", "12-02-2019"));
inMemory.add(RowFactory.create("FATAL", "13-10-2019"));

// define the schema of the dataset (row)
StructField[] fields = new StructField[]{
        new StructField("level", DataTypes.StringType, false, Metadata.empty()),
        new StructField("datetime", DataTypes.StringType, false, Metadata.empty())
};
StructType schema = new StructType(fields);

// Use createDataFrame to create a Dataset using the array data and the defined schema
Dataset<Row> dataset = spark.createDataFrame(inMemory, schema);

dataset.createOrReplaceTempView("logging_table");

// Run SQL query in the table view of the in-memory array list
Dataset<Row> result = spark.sql("select level, date_format(datetime,'MMMM') as month, count(1) as total from logging_table group by level, month");

result.createOrReplaceTempView("result_table");

Dataset<Row> sum = spark.sql("select sum(total) from result_table");
sum.show();`

const hiveCodeExample = `public class HiveExample {

    public static void main(String[] args) {
        String warehouseLocation = new File("spark-warehouse").getAbsolutePath();
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark Hive Example")
                .master("local")
                .config("hive.metastore.uri", "thrift://localhost:9083")
                .enableHiveSupport()
                .getOrCreate();

        spark.sqlContext().sql("CREATE DATABASE IF NOT EXISTS prime_tut");
        spark.sqlContext().sql("USE prime_tut");
        spark.sqlContext().sql("CREATE TABLE IF NOT EXISTS src (key INT, value STRING) USING hive");
        spark.sqlContext().sql("LOAD DATA LOCAL INPATH 'src/main/resources/key_value.txt' INTO TABLE src");

        // Queries are expressed in HiveQL
        spark.sqlContext().sql("SELECT * FROM src").show();

        // Create DS from CSV
        Dataset<Row> students = spark.read().option("header", "true").csv("src/main/resources/students.csv");

        // Write DS to hive
        students.write().mode("overwrite").saveAsTable("students");


        spark.stop();
    }
}`
class SparkSQL extends React.Component {
  render() {
    const { classes, section } = this.props
    const hiveExample = section.children[0]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>
        <Typography>
          The sql function on a SparkSession enables applications to run SQL queries programmatically and returns the result as a Dataset.
          <Code>
            {sqlExample}
          </Code>
          <Bold>Exercise 6</Bold>: Create a Dataset by reading the file students.csv, using SQL statement, filter by subject 'Modern Art' and year 2007.
        </Typography>
        <Exercise url={'/lecture3/exercise6'} />
        <Typography id={hiveExample.id} variant={'title'}>
          {hiveExample.display}
        </Typography>
        <Code>
          {hiveCodeExample}
        </Code>
      </Fragment>
    )
  }
}

export default withStyles(styles)(SparkSQL)
