/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import JavaAssignments from "pages/lecture3/JavaAssignments";
import DataAssignments from "pages/lecture3/DataAssignments";
import React from "react";
import Intro from "pages/lecture3/Intro";
import SparkSQL from "pages/lecture3/SparkSQL";
import SparkRDD from "pages/lecture3/SparkRDD";
import SparkDataset from "pages/lecture3/SparkDataset";
import Spark from "pages/lecture3/Spark";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_3.INTRODUCTION:
        return <Spark {...props} />
      case PAGES.LECTURE_3.SPARK_RDD:
        return <SparkRDD {...props} />
      case PAGES.LECTURE_3.SPARK_DATASET:
        return <SparkDataset {...props} />
      case PAGES.LECTURE_3.SPARK_SQL:
        return <SparkSQL {...props} />
      case PAGES.LECTURE_3.SPARK_JAVA_ASSIGNMENTS:
        return <JavaAssignments {...props} />
      case PAGES.LECTURE_3.SPARK_DATA_ASSIGNMENTS:
        return <DataAssignments {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
