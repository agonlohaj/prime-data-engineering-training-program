/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from 'presentations/Divider';
import Typography from 'presentations/Typography';
import React, { Fragment } from "react";

const styles = ({ typography }) => ({
  root: {},
})

/**
 * Home Page component
 */
class Home extends React.Component {
  render() {
    const { classes } = this.props
    return (
      <Fragment>
        <Typography variant={'heading'}>
          PRIME Data Engineering Training Program
          <Typography>Designed and structured by PRIME Data Team</Typography>
          <Divider />
        </Typography>
        <Typography variant={'p'}>
          Welcome to the data training program by PRIME. This web app will be the container of all the materials that
          you need to have in order to follow through the training. We are going to publish every material that we use
          during our lectures, related assignments and also resources that you will need to complete the training
          successfully.
        </Typography>

        <Typography variant={'p'}>
          This is an interactive web app, and we build it in such a manner that is transparent to the participant
          following this. We are going to show code snippets, partial or complete code samples, examples, content and
          resources.
        </Typography>

        <Typography>
          We this interactive web app has some tricks up its sleeves (you’ll get to experience them very soon,
          “INSERT EVIL LAUGH”). The design guidelines of this web app are based on our own PRIME guidelines (you will get to use them often).
          The project setup contains all the relevant libraries that you are going to be using, however you may add more library references
          if necessary, although we believe that the ones provided will suffice.
        </Typography>

        <Typography>
          By the time you are reading this on your own laptop,
          you have already set it up (the instructions will be given soon, see “Getting Started” session)
        </Typography>

        <Typography variant={'p'}>
          The lectures will be taught by PRIME Data Engineers and they will take place at the offices at Prime in Pristina.
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(Home)
