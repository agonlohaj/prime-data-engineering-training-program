/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import {Bold, Italic} from "presentations/Label";
import EmrClusterImg from 'assets/images/lecture7/emr_cluster.png'
import EmrStepSequenceImg from 'assets/images/lecture7/emr_step_sequence.png'
import EmrStepSequenceFailedImg from 'assets/images/lecture7/emr_step_sequence_failed.png'

const styles = ({ typography }) => ({
  root: {},
})


class EmrIntro extends React.Component {
  render() {
    const { classes, section } = this.props
    const benefits = section.children[0]
    const understandingEmr = section.children[1]
    const submittingWork = section.children[2]
    const processingData = section.children[3]
    const lifecycle = section.children[4]

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography>
          Amazon EMR (Elastic MapReduce) is a managed cluster platform that simplifies running big data frameworks,
          allowing teams to process vast amounts of data quickly, and cost-effectively at scale. Using open source
          tools such as Apache Spark, Apache Hive, Apache HBase, Apache Flink, and Presto, coupled with the dynamic
          scalability of Amazon EC2 and scalable storage of Amazon S3, EMR gives analytical teams the engines and
          elasticity to run Petabyte-scale analysis for a fraction of the cost of traditional on-premise clusters.
          Additionally, you can use Amazon EMR to transform and move large amounts of data into and out of other
          AWS data stores and databases, such as Amazon Simple Storage Service (Amazon S3) and Amazon DynamoDB.
        </Typography>

        <Typography>
          Customers across many industry verticals use EMR to securely and reliably handle broad sets of big data
          use cases, including machine learning, data transformations (ETL), financial and scientific simulation,
          bioinformatics, log analysis, and deep learning. EMR gives teams the flexibility to run use cases on
          single-purpose short lived clusters that automatically scale to meet demand, or on long running highly
          available clusters using the new multi-master deployment mode.
        </Typography>

        <Typography id={benefits.id} variant={'title'}>
          {benefits.display}
        </Typography>

        <Typography>
          <ul>
            <li>
              <Bold>
                Easy to use:
              </Bold>
              EMR launches clusters in minutes. You don’t need to worry about node provisioning, infrastructure setup,
              Hadoop configuration, or cluster tuning. EMR takes care of these tasks so you can focus on analysis.
            </li>
            <li>
              <Bold>
                Low Cost:
              </Bold>
              EMR pricing is simple and predictable: You pay a per-instance rate for every second used, with a
              one-minute minimum charge. Also the user of EMR has the option to chose from various EC2 instance
              types and purchasing options, which allows you to manage your costs better and ensure that the cluster
              has what it needs to run more efficiently.
            </li>
            <li>
              <Bold>
                Elastic:
              </Bold>
              With EMR, you can provision one, hundreds, or thousands of compute instances to process data at any
              scale. The number of instances can be increased or decreased manually or automatically using Auto
              Scaling (which manages cluster sizes based on utilization), and you only pay for what you use. Unlike
              the rigid infrastructure of on-premise clusters, EMR decouples compute and persistent storage, giving
              you the ability to scale each independently.
            </li>
            <li>
              <Bold>
                Reliable:
              </Bold>
              Spend less time tuning and monitoring your cluster. EMR is tuned for the cloud, and constantly monitors
              your cluster — retrying failed tasks and automatically replacing poorly performing instances. EMR
              provides the latest stable open source software releases, so you don’t have to manage updates and bug
              fixes, leading to fewer issues and less effort to maintain the environment. With multiple master nodes,
              clusters are highly available and automatically failover in the event of a node failure.
            </li>
            <li>
              <Bold>
                Secure:
              </Bold>
              EMR automatically configures EC2 firewall settings controlling network access to instances, and
              launches clusters in an Amazon Virtual Private Cloud (VPC), a logically isolated network you define.
              For objects stored in S3, server-side encryption or client-side encryption can be used with EMRFS
              (an object store for Hadoop on S3), using the AWS Key Management Service or your own customer-managed
              keys. EMR makes it easy to enable other encryption options, like in-transit and at-rest encryption,
              and strong authentication with Kerberos.
            </li>
            <li>
              <Bold>
                Flexible:
              </Bold>
              You have complete control over your cluster. You have root access to every instance, you can easily
              install additional applications, and customize every cluster with bootstrap actions. You can also
              launch EMR clusters with custom Amazon Linux AMIs, and reconfigure running clusters on the fly without
              the need to re-launch the cluster.
            </li>
          </ul>
        </Typography>

        <Typography id={understandingEmr.id} variant={'title'}>
          {understandingEmr.display}
        </Typography>

        <Typography>
          The central component of Amazon EMR is the cluster. A cluster is a collection of Amazon Elastic Compute Cloud
          (Amazon EC2) instances. Each instance in the cluster is called a node. Each node has a role within the
          cluster, referred to as the node type. Amazon EMR also installs different software components on each
          node type, giving each node a role in a distributed application like Apache Hadoop.
        </Typography>

        <Typography>
          The node types in Amazon EMR are as follows:
          <ul>
            <li>
              <Bold>
                Master node:
              </Bold>
              A node that manages the cluster by running software components to coordinate the distribution of data
              and tasks among other nodes for processing. The master node tracks the status of tasks and monitors the
              health of the cluster. Every cluster has a master node, and it's possible to create a single-node
              cluster with only the master node.
            </li>
            <li>
              <Bold>
                Core node:
              </Bold>
              A node with software components that run tasks and store data in the Hadoop Distributed File System
              (HDFS) on your cluster. Multi-node clusters have at least one core node.
            </li>
            <li>
              <Bold>
                Task node:
              </Bold>
              A node with software components that only runs tasks and does not store data in HDFS. Task nodes are optional.
            </li>
          </ul>
        </Typography>

        <Typography>
          The following diagram represents a cluster with one master node and four core nodes:
        </Typography>

        <Typography>
          <img src={EmrClusterImg}/>
        </Typography>

        <Typography id={submittingWork.id} variant={'title'}>
          {submittingWork.display}
        </Typography>

        <Typography>
          When you run a cluster on Amazon EMR, you have several options as to how you specify the work that needs to be done.
          <ul>
            <li>
              Provide the entire definition of the work to be done in functions that you specify as steps when you
              create a cluster. This is typically done for clusters that process a set amount of data and then
              terminate when processing is complete.
            </li>
            <li>
              Create a long-running cluster and use the Amazon EMR console, the Amazon EMR API, or the AWS CLI to
              submit steps, which may contain one or more jobs.
            </li>
            <li>
              Create a cluster, connect to the master node and other nodes as required using SSH, and use the
              interfaces that the installed applications provide to perform tasks and submit queries, either
              scripted or interactively.
            </li>
          </ul>
        </Typography>

        <Typography id={processingData.id} variant={'title'}>
          {processingData.display}
        </Typography>

        <Typography>
          When you launch your cluster, you choose the frameworks and applications to install for your data processing
          needs. To process data in your Amazon EMR cluster, you can submit jobs or queries directly to installed
          applications, or you can run steps in the cluster.
        </Typography>

        <Typography>
          Submitting Jobs Directly to Applications
        </Typography>

        <Typography>
          You can submit jobs and interact directly with the software that is installed in your Amazon EMR cluster.
          To do this, you typically connect to the master node over a secure connection and access the interfaces
          and tools that are available for the software that runs directly on your cluster. For more information,
          see Connect to the Cluster.
        </Typography>

        <Typography>
          Running Steps to Process Data
        </Typography>

        <Typography>
          You can submit one or more ordered steps to an Amazon EMR cluster. Each step is a unit of work that contains
          instructions to manipulate data for processing by software installed on the cluster.
        </Typography>

        <Typography>
          Steps are run in the following sequence:
          <ul>
            <li>
              A request is submitted to begin processing steps.
            </li>
            <li>
              The state of all steps is set to <Bold>PENDING</Bold>.
            </li>
            <li>
              When the first step in the sequence starts, its state changes to <Bold>RUNNING</Bold>. The other steps
              remain in the <Bold>PENDING</Bold> state.
            </li>
            <li>
              After the first step completes, its state changes to <Bold>COMPLETED</Bold>.
            </li>
            <li>
              The next step in the sequence starts, and its state changes to <Bold>RUNNING</Bold>. When it completes,
              its state changes to <Bold>COMPLETED</Bold>.
            </li>
            <li>
              This pattern repeats for each step until they all complete and processing ends.
            </li>
          </ul>
        </Typography>

        <Typography>
          The following diagram represents the step sequence and change of state for the steps as they are processed:
        </Typography>

        <Typography>
          <img src={EmrStepSequenceImg} />
        </Typography>

        <Typography>
          If a step fails during processing, its state changes to <Bold>TERMINATED_WITH_ERRORS</Bold>. You can determine what
          happens next for each step. By default, any remaining steps in the sequence are set to <Bold>CANCELLED</Bold> and do not
          run. You can also choose to ignore the failure and allow remaining steps to proceed, or to terminate the
          cluster immediately.
        </Typography>

        <Typography>
          The following diagram represents the step sequence and default change of state when a step fails during processing:
        </Typography>

        <Typography>
          <img src={EmrStepSequenceFailedImg} />
        </Typography>

        <Typography id={lifecycle.id} variant={'title'}>
          {lifecycle.display}
        </Typography>

        <Typography>
          A successful Amazon EMR cluster follows this process:
          <ul>
            <li>
              Amazon EMR first provisions EC2 instances in the cluster for each instance according to your
              specifications. For all instances, Amazon EMR uses the default AMI for Amazon EMR or a custom
              Amazon Linux AMI that you specify. During this phase, the cluster state is <Bold>STARTING</Bold>.
            </li>
            <li>
              Amazon EMR runs bootstrap actions that you specify on each instance. You can use bootstrap actions to
              install custom applications and perform customizations that you require. During this phase, the
              cluster state is <Bold>BOOTSTRAPPING</Bold>.
            </li>
            <li>
              Amazon EMR installs the native applications that you specify when you create the cluster, such as
              Hive, Hadoop, Spark, and so on.
            </li>
            <li>
              After bootstrap actions are successfully completed and native applications are installed, the cluster
              state is <Bold>RUNNING</Bold>. At this point, you can connect to cluster instances, and the cluster sequentially
              runs any steps that you specified when you created the cluster. You can submit additional steps,
              which run after any previous steps complete.
            </li>
            <li>
              After steps run successfully, the cluster goes into a <Bold>WAITING</Bold> state. If a cluster is configured to
              auto-terminate after the last step is complete, it goes into a <Bold>SHUTTING_DOWN</Bold> state.
            </li>
            <li>
              After all instances are terminated, the cluster goes into the <Bold>COMPLETED</Bold> state.
            </li>
          </ul>
        </Typography>

        <Typography>
          A failure during the cluster lifecycle causes Amazon EMR to terminate the cluster and all of its instances
          unless you enable termination protection. If a cluster terminates because of a failure, any data stored on
          the cluster is deleted, and the cluster state is set to <Bold>FAILED</Bold>.
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(EmrIntro)
