/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import PageLink from "presentations/rows/nav/PageLink";

const styles = ({ typography }) => ({
  root: {},
})


class EmrArchitecture extends React.Component {
  render() {
    const { classes, section } = this.props
    const storage = section.children[0]
    const clusterResourceManagement = section.children[1]
    const dataProcessingFrameworks = section.children[2]
    const applicationsAndPrograms = section.children[3]

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography>
          Amazon EMR service architecture consists of several layers, each of which provides certain capabilities
          and functionality to the cluster. This section provides an overview of the layers and the components of each.
        </Typography>

        <Typography>
          In this section we are going to focus on:
          <ul>
            <li>Storage</li>
            <li>Cluster Resource Management</li>
            <li>Data Processing Frameworks</li>
            <li>Applications and Programs</li>
          </ul>
        </Typography>

        <Typography id={storage.id} variant={'title'}>
          {storage.display}
        </Typography>

        <Typography>
          The storage layer includes the different file systems that are used with your cluster. There are several
          different types of storage options as follows:
          <ul>
            <li>
              Hadoop Distributed File System (HDFS) - Introduced at <PageLink to={`/section/hadoop/`}> Hadoop</PageLink> Chapter
            </li>
            <li>
              EMR File System (EMRFS) - Using the EMR File System (EMRFS), Amazon EMR extends Hadoop to add the
              ability to directly access data stored in Amazon S3 as if it were a file system like HDFS. You can use
              either HDFS or Amazon S3 as the file system in your cluster. Most often, Amazon S3 is used to store
              input and output data and intermediate results are stored in HDFS.
            </li>
            <li>
              Local File System - The local file system refers to a locally connected disk. When you create a Hadoop
              cluster, each node is created from an Amazon EC2 instance that comes with a preconfigured block of
              pre-attached disk storage called an instance store. Data on instance store volumes persists only during
              the lifecycle of its Amazon EC2 instance.
            </li>
          </ul>
        </Typography>

        <Typography id={clusterResourceManagement.id} variant={'title'}>
          {clusterResourceManagement.display}
        </Typography>

        <Typography>
          The resource management layer is responsible for managing cluster resources and scheduling the jobs for
          processing data.
        </Typography>

        <Typography>
          By default, Amazon EMR uses YARN (Yet Another Resource Negotiator, Yarn). However, there are other
          frameworks and applications that are offered in Amazon EMR that do not use YARN as a resource manager.
          Amazon EMR also has an agent on each node that administers YARN components, keeps the cluster healthy,
          and communicates with Amazon EMR.
        </Typography>

        <Typography>
          Because Spot Instances are often used to run task nodes, Amazon EMR has default functionality for
          scheduling YARN jobs so that running jobs don’t fail when task nodes running on Spot Instances are
          terminated. Amazon EMR does this by allowing application master processes to run only on core nodes.
          The application master process controls running jobs and needs to stay alive for the life of the job.
        </Typography>

        <Typography id={dataProcessingFrameworks.id} variant={'title'}>
          {dataProcessingFrameworks.display}
        </Typography>

        <Typography>
          The data processing framework layer is the engine used to process and analyze data. There are many frameworks
          available that run on YARN or have their own resource management. Different frameworks are available for
          different kinds of processing needs, such as batch, interactive, in-memory, streaming, and so on. The
          framework that you choose depends on your use case. This impacts the languages and interfaces available
          from the application layer, which is the layer used to interact with the data you want to process. The
          main processing frameworks available for Amazon EMR are:
          <ul>
            <li>Hadoop MapReduce</li>
            <li>Apache Spark</li>
          </ul>
        </Typography>

        <Typography id={applicationsAndPrograms.id} variant={'title'}>
          {applicationsAndPrograms.display}
        </Typography>

        <Typography>
          Amazon EMR supports many applications, such as Hive, Pig, and the Spark Streaming library to provide
          capabilities such as using higher-level languages to create processing workloads, leveraging machine
          learning algorithms, making stream processing applications, and building data warehouses. In addition,
          Amazon EMR also supports open-source projects that have their own cluster management functionality
          instead of using YARN.
        </Typography>

        <Typography>
          You use various libraries and languages to interact with the applications that you run in Amazon EMR.
          For example, you can use Java, Hive, or Pig with MapReduce or Spark Streaming, Spark SQL, MLlib, and
          GraphX with Spark.
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(EmrArchitecture)
