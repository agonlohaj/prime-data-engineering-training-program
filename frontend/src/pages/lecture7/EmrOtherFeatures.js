/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import {Bold, Italic} from "presentations/Label";
import SimpleLink from "presentations/rows/SimpleLink";

const styles = ({ typography }) => ({
  root: {},
})


class EmrOtherFeatures extends React.Component {
  render() {
    const { classes, section } = this.props
    const awsIntegration = section.children[0]
    const scalabilityAndFlexibility = section.children[1]
    const reliability = section.children[2]
    const security = section.children[3]
    const monitoring = section.children[4]
    const managementInterfaces = section.children[5]

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography id={awsIntegration.id} variant={'title'}>
          {awsIntegration.display}
        </Typography>

        <Typography>
          Amazon EMR integrates with other AWS services to provide capabilities and functionality related to
          networking, storage, security, and so on, for your cluster. The following list provides several examples
          of this integration:
          <ul>
            <li>Amazon EC2 for the instances that comprise the nodes in the cluster</li>
            <li>Amazon Virtual Private Cloud (Amazon VPC) to configure the virtual network in which you launch your instances</li>
            <li>Amazon S3 to store input and output data</li>
            <li>
              Amazon CloudWatch to monitor cluster performance and configure alarms</li>
            <li>AWS Identity and Access Management (IAM) to configure permissions</li>
            <li>AWS CloudTrail to audit requests made to the service</li>
            <li>AWS Data Pipeline to schedule and start your clusters</li>
          </ul>
        </Typography>

        <Typography id={scalabilityAndFlexibility.id} variant={'title'}>
          {scalabilityAndFlexibility.display}
        </Typography>

        <Typography>
          Amazon EMR provides flexibility to scale your cluster up or down as your computing needs change. You can
          resize your cluster to add instances for peak workloads and remove instances to control costs when peak
          workloads subside. For more information, see Manually Resizing a Running Cluster.
        </Typography>

        <Typography>
          Amazon EMR also provides the option to run multiple instance groups so that you can use On-Demand Instances
          in one group for guaranteed processing power together with Spot Instances in another group to have your
          jobs completed faster and for lower costs. You can also mix different instance types to take advantage of
          better pricing for one Spot Instance type over another.
        </Typography>

        <Typography id={reliability.id} variant={'title'}>
          {reliability.display}
        </Typography>

        <Typography>
          Amazon EMR monitors nodes in your cluster and automatically terminates and replaces an instance in case of failure.
        </Typography>

        <Typography>
          Amazon EMR provides configuration options that control how your cluster is terminated—automatically or
          manually. If you configure your cluster to be automatically terminated, it is terminated after all the
          steps complete. This is referred to as a transient cluster. However, you can configure the cluster to
          continue running after processing completes so that you can choose to terminate it manually when you no
          longer need it. Or, you can create a cluster, interact with the installed applications directly, and then
          manually terminate the cluster when you no longer need it. The clusters in these examples are referred
          to as long-running clusters.
        </Typography>

        <Typography id={security.id} variant={'title'}>
          {security.display}
        </Typography>

        <Typography>
          Amazon EMR leverages other AWS services, such as IAM and Amazon VPC, and features such as Amazon EC2 key
          pairs, to help you secure your clusters and data.
          <ul>
            <li>
              <Bold>IAM: </Bold>
              Amazon EMR integrates with IAM to manage permissions. You define permissions using IAM policies, which
              you attach to IAM users or IAM groups. The permissions that you define in the policy determine the
              actions that those users or members of the group can perform and the resources that they can access.
            </li>
            <li>
              <Bold>Security Groups: </Bold>
              Amazon EMR uses security groups to control inbound and outbound traffic to your EC2 instances. When you
              launch your cluster, Amazon EMR uses a security group for your master instance and a security group to
              be shared by your core/task instances. Amazon EMR configures the security group rules to ensure
              communication among the instances in the cluster.
            </li>
            <li>
              <Bold>Encryption: </Bold>
              Amazon EMR supports optional Amazon S3 server-side and client-side encryption with EMRFS to help
              protect the data that you store in Amazon S3. With server-side encryption, Amazon S3 encrypts your
              data after you upload it.
            </li>
            <li>
              <Bold>Amazon VPC: </Bold>
              Amazon EMR supports launching clusters in a virtual private cloud (VPC) in Amazon VPC. A VPC is an
              isolated, virtual network in AWS that provides the ability to control advanced aspects of network
              configuration and access.
            </li>
            <li>
              <Bold>AWS CloudTrail: </Bold>
              Amazon EMR integrates with CloudTrail to log information about requests made by or on behalf of your AWS
              account. With this information, you can track who is accessing your cluster when, and the IP address
              from which they made the request.
            </li>
            <li>
              <Bold>Amazon EC2 Key Pairs: </Bold>
              You can monitor and interact with your cluster by forming a secure connection between your remote
              computer and the master node. You use the Secure Shell (SSH) network protocol for this connection
              or use Kerberos for authentication.
            </li>
          </ul>
        </Typography>

        <Typography id={monitoring.id} variant={'title'}>
          {monitoring.display}
        </Typography>

        <Typography>
          You can use the Amazon EMR management interfaces and log files to troubleshoot cluster issues, such as
          failures or errors. Amazon EMR provides the ability to archive log files in Amazon S3 so you can store logs
          and troubleshoot issues even after your cluster terminates. Amazon EMR also provides an optional debugging
          tool in the Amazon EMR console to browse the log files based on steps, jobs, and tasks.
        </Typography>

        <Typography>
          Amazon EMR integrates with CloudWatch to track performance metrics for the cluster and jobs within the
          cluster. You can configure alarms based on a variety of metrics such as whether the cluster is idle or
          the percentage of storage used.
        </Typography>

        <Typography id={managementInterfaces.id} variant={'title'}>
          {managementInterfaces.display}
        </Typography>

        <Typography>
          There are several ways you can interact with Amazon EMR:
          <ul>
            <li>
              <Bold>Console - </Bold>
              A graphical user interface that you can use to launch and manage clusters. With it, you fill out web
              forms to specify the details of clusters to launch, view the details of existing clusters, debug, and
              terminate clusters. Using the console is the easiest way to get started with Amazon EMR; no programming
              knowledge is required. The console is available online at the following
              <SimpleLink href={'https://eu-central-1.console.aws.amazon.com/elasticmapreduce/home?region=eu-central-1'}> link</SimpleLink>
            </li>
            <li>
              <Bold>AWS Command Line Interface (AWS CLI) - </Bold>
              A client application you run on your local machine to connect to Amazon EMR and create and manage
              clusters. The AWS CLI contains a feature-rich set of commands specific to Amazon EMR. With it, you
              can write scripts that automate the process of launching and managing clusters. If you prefer working
              from a command line, using the AWS CLI is the best option.
            </li>
            <li>
              <Bold>Software Development Kit (SDK) - </Bold>
              SDKs provide functions that call Amazon EMR to create and manage clusters. With them, you can write
              applications that automate the process of creating and managing clusters. Using the SDK is the best
              option to extend or customize the functionality of Amazon EMR. Amazon EMR is currently available in
              the following SDKs: Go, Java, .NET (C# and VB.NET), Node.js, PHP, Python, and Ruby.
            </li>
            <li>
              <Bold>Web Service API - </Bold>
              A low-level interface that you can use to call the web service directly, using JSON. Using the API is
              the best option to create a custom SDK that calls Amazon EMR
            </li>
          </ul>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(EmrOtherFeatures)
