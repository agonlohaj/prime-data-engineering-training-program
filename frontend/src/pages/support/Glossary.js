/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import { Bold } from 'presentations/Label'
const styles = ({ typography }) => ({
  root: {},
})

class Glossary extends React.Component {
  render() {
    const { classes } = this.props
    return (
      <Fragment>
        <Typography variant={'heading'}>
          Glossary
          <Divider />
        </Typography>
        <Typography variant='p'>
          <ol>
            <li>
              <Bold>Big data</Bold>: Big data is an umbrella term for datasets that cannot reasonably be handled by traditional computers or tools due to their volume, velocity, and variety. This term is also typically applied to technologies and strategies to work with this type of data.
            </li>
            <li>
              <Bold>Batch processing</Bold>: Batch processing is a computing strategy that involves processing data in large sets. This is typically ideal for non-time sensitive work that operates on very large sets of data. The process is started and at a later time, the results are returned by the system.
            </li>
            <li>
              <Bold>Cluster computing</Bold>: Clustered computing is the practice of pooling the resources of multiple machines and managing their collective capabilities to complete tasks. Computer clusters require a cluster management layer which handles communication between the individual nodes and coordinates work assignment.
            </li>
            <li>
              <Bold>Data lake</Bold>: Data lake is a term for a large repository of collected data in a relatively raw state. This is frequently used to refer to the data collected in a big data system which might be unstructured and frequently changing. This differs in spirit to data warehouses (defined below).
            </li>
            <li>
              <Bold>Data mining</Bold>: Data mining is a broad term for the practice of trying to find patterns in large sets of data. It is the process of trying to refine a mass of data into a more understandable and cohesive set of information.
            </li>
            <li>
              <Bold>Data warehouse</Bold>: Data warehouses are large, ordered repositories of data that can be used for analysis and reporting. In contrast to a data lake, a data warehouse is composed of data that has been cleaned, integrated with other sources, and is generally well-ordered. Data warehouses are often spoken about in relation to big data, but typically are components of more conventional systems.
            </li>
            <li>
              <Bold>ETL</Bold>: ETL stands for extract, transform, and load. It refers to the process of taking raw data and preparing it for the system's use. This is traditionally a process associated with data warehouses, but characteristics of this process are also found in the ingestion pipelines of big data systems.
            </li>
            <li>
              <Bold>Hadoop</Bold>: Hadoop is an Apache project that was the early open-source success in big data. It consists of a distributed filesystem called HDFS, with a cluster management and resource scheduler on top called YARN (Yet Another Resource Negotiator). Batch processing capabilities are provided by the MapReduce computation engine. Other computational and analysis systems can be run alongside MapReduce in modern Hadoop deployments.
            </li>
            <li>
              <Bold>In-memory computing</Bold>: In-memory computing is a strategy that involves moving the working datasets entirely within a cluster's collective memory. Intermediate calculations are not written to disk and are instead held in memory. This gives in-memory computing systems like Apache Spark a huge advantage in speed over I/O bound systems like Hadoop's MapReduce.
            </li>
            <li>
              <Bold>Machine learning</Bold>: Machine learning is the study and practice of designing systems that can learn, adjust, and improve based on the data fed to them. This typically involves implementation of predictive and statistical algorithms that can continually zero in on "correct" behavior and insights as more data flows through the system.
            </li>
            <li>
              <Bold>Map reduce (big data algorithm)</Bold>: Map reduce (the big data algorithm, not Hadoop's MapReduce computation engine) is an algorithm for scheduling work on a computing cluster. The process involves splitting the problem set up (mapping it to different nodes) and computing over them to produce intermediate results, shuffling the results to align like sets, and then reducing the results by outputting a single value for each set.
            </li>
            <li>
              <Bold>Stream processing</Bold>: Stream processing is the practice of computing over individual data items as they move through a system. This allows for real-time analysis of the data being fed to the system and is useful for time-sensitive operations using high velocity metrics.
            </li>
            <li><Bold>CD</Bold> - Continuous Development</li>
            <li><Bold>CI</Bold> - Continuous Integration</li>
            <li><Bold>OOP</Bold> - Object Oriented Programming</li>
            <li><Bold>NPM</Bold> - Node Package Manager for the Javascript programming language</li>
            <li><Bold>VS</Bold> - Visual Studio</li>
            <li><Bold>GUI</Bold> - Graphical User Interface</li>
            <li><Bold>JSON</Bold> - JavaScript Object Notation</li>
            <li><Bold>QA</Bold> - Quality Assurance</li>
            <li><Bold>API</Bold> - Application programming interface</li>
            <li><Bold>CSS</Bold> - Cascading Style Sheet</li>
            <li><Bold>UI</Bold> - User Interface</li>
            <li><Bold>REST</Bold> - REpresentational State Transfer</li>
            <li><Bold>SBT</Bold> - Scala Build Tools</li>
          </ol>
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(Glossary)
