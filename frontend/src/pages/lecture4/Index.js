/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import { PAGES } from 'Constants';
import React from "react";
import Intro from "pages/lecture4/Intro";
import Assignments from "pages/lecture4/Assignments";
import SelectStatement from "pages/lecture4/SelectStatement";
import Aggregations from "pages/lecture4/Aggregations";
import Joins from "pages/lecture4/Joins";
import Design from "pages/lecture4/Design";
import DataTypes from "pages/lecture4/DataTypes";

const styles = () => ({
  root: {},
})

class Index extends React.Component {
  render() {
    const { breadcrumbs } = this.props

    let section = breadcrumbs[0]
    if (breadcrumbs.length > 1) {
      section = breadcrumbs[1]
    }

    const props = {
      section
    }

    switch (section.id) {
      case PAGES.LECTURE_4.SELECT:
        return <SelectStatement {...props} />
      case PAGES.LECTURE_4.AGGREGATE:
        return <Aggregations {...props} />
      case PAGES.LECTURE_4.JOIN:
        return <Joins {...props} />
      case PAGES.LECTURE_4.ASSIGNMENTS:
        return <Assignments {...props} />
      case PAGES.LECTURE_4.DESIGN:
        return <Design {...props} />
      case PAGES.LECTURE_4.DATA_TYPES:
        return <DataTypes {...props} />
    }
    return <Intro {...props} />
  }
}

export default withStyles(styles)(Index)
