/**
 * Created by Labinot Vila on 29/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, {Fragment} from "react";
import {Bold} from "presentations/Label";
import {Table, TableBody, TableCell, TableHead, TableRow} from "@material-ui/core";
import Code from "presentations/Code";

const styles = ({typography}) => ({
    root: {},
})

const constraintCode = `CREATE TABLE table_name
                        (
                            column1 datatype constraint,
                            column2 datatype constraint,
                            column3 datatype constraint, .
                            .
                            .
                        );
`

class DataTypes extends React.Component {
    render() {
        const {classes, section} = this.props

        const constraints = section.children[0]


        return (
            <Fragment>
                <Typography variant={'heading'}>
                    {section.display}
                    <Divider/>
                </Typography>

                <Typography>
                    <Bold>String Data Types</Bold>
                </Typography>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Data Types</TableCell>
                            <TableCell>Description</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>CHAR(size)</TableCell>
                            <TableCell>A FIXED length string (can contain letters, numbers, and special characters), can
                                be
                                from 0 to 255. Default is 1</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>VARCHAR(size)</TableCell>
                            <TableCell>A VARIABLE length string (can contain letters, numbers, and special characters),
                                can
                                be from 0 to 65535</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>BINARY(size)</TableCell>
                            <TableCell>Equal to CHAR(), but stores binary byte strings. The size parameter specifies the
                                column
                                length in bytes. Default is 1</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>VARBINARY(size)</TableCell>
                            <TableCell>Equal to VARCHAR(), but stores binary byte strings. The size parameter specifies
                                the
                                maximum column length in bytes.</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>TINYBLOB</TableCell>
                            <TableCell>For BLOBs (Binary Large OBjects). Max length: 255 bytes</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>TINYTEXT</TableCell>
                            <TableCell>Holds a string with a maximum length of 255 characters</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>TEXT(size)</TableCell>
                            <TableCell>Holds a string with a maximum length of 65,535 bytes</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>BLOB(size)</TableCell>
                            <TableCell>For BLOBs (Binary Large OBjects). Holds up to 65,535 bytes of data s</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>MEDIUMTEXT</TableCell>
                            <TableCell>Holds a string with a maximum length of 16,777,215 characters</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>MEDIUMBLOB</TableCell>
                            <TableCell>For BLOBs (Binary Large OBjects). Holds up to 16,777,215 bytes of
                                data</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>LONGTEXT</TableCell>
                            <TableCell>Holds a string with a maximum length of 4,294,967,295 characters</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>LONGBLOB</TableCell>
                            <TableCell>For BLOBs (Binary Large OBjects). Holds up to 4,294,967,295 bytes of
                                data</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>ENUM(val1, val2, val3, ...)</TableCell>
                            <TableCell>A string object that can have only one value, chosen from a list of possible
                                values.
                                You can list up to 65535 values in an ENUM list. If a value is inserted that is not in
                                the
                                list, a blank value will be inserted. The values are sorted in the order you enter
                                them</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>SET(val1, val2, val3, ...)</TableCell>
                            <TableCell>A string object that can have 0 or more values, chosen from a list of possible
                                values.
                                You can list up to 64 values in a SET list</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>

                <Typography>
                    <Bold>String Data Types</Bold>
                </Typography>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Data Types</TableCell>
                            <TableCell>Description</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>BIT(size)</TableCell>
                            <TableCell>A bit-value type. The number of bits per value is specified in size. The size
                                parameter
                                can hold a value from 1 to 64. The default value for size is 1.</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>TINYINT(size)</TableCell>
                            <TableCell>A very small integer. Signed range is from -128 to 127. Unsigned range is from 0
                                to 255.
                                The size parameter specifies the maximum display width (which is 255)</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>BOOL</TableCell>
                            <TableCell>Zero is considered as false, nonzero values are considered as true.</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>BOOLEAN</TableCell>
                            <TableCell>Equal to BOOL</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>SMALLINT(size)</TableCell>
                            <TableCell>A small integer. Signed range is from -32768 to 32767. Unsigned range is from 0
                                to 65535.
                                The size parameter specifies the maximum display width (which is 255)</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>MEDIUMINT(size)</TableCell>
                            <TableCell>A medium integer. Signed range is from -8388608 to 8388607. Unsigned range is
                                from 0 to
                                16777215. The size parameter specifies the maximum display width (which is
                                255)</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>INT(size)</TableCell>
                            <TableCell>A medium integer. Signed range is from -2147483648 to 2147483647. Unsigned range
                                is
                                from 0 to 4294967295. The size parameter specifies the maximum display width (which is
                                255)</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>INTEGER(size)</TableCell>
                            <TableCell>Equal to INT(size)</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>BIGINT(size)</TableCell>
                            <TableCell>A large integer. Signed range is from -9223372036854775808 to
                                9223372036854775807.
                                Unsigned range is from 0 to 18446744073709551615. The size parameter specifies the
                                maximum
                                display width (which is 255)</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>FLOAT(size, d)</TableCell>
                            <TableCell>A floating point number. The total number of digits is specified in size. The
                                number
                                of digits after the decimal point is specified in the d parameter. This syntax is
                                deprecated
                                in MySQL 8.0.17, and it will be removed in future MySQL versions</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>FLOAT(p)</TableCell>
                            <TableCell>A floating point number. MySQL uses the p value to determine whether to use FLOAT
                                or
                                DOUBLE for the resulting data type. If p is from 0 to 24, the data type becomes FLOAT().
                                If
                                p is from 25 to 53, the data type becomes DOUBLE()</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>DOUBLE(size, d)</TableCell>
                            <TableCell>A normal-size floating point number. The total number of digits is specified in
                                size.
                                The number of digits after the decimal point is specified in the d parameter</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>DOUBLE PRECISION(size, d)</TableCell>
                            <TableCell>A string object that can have only one value, chosen from a list of possible
                                values.
                                You can list up to 65535 values in an ENUM list. If a value is inserted that is not in
                                the
                                list, a blank value will be inserted. The values are sorted in the order you enter
                                them</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>DECIMAL(size, d)</TableCell>
                            <TableCell>An exact fixed-point number. The total number of digits is specified in size. The
                                number of digits after the decimal point is specified in the d parameter. The maximum
                                number
                                for size is 65. The maximum number for d is 30. The default value for size is 10. The
                                default
                                value for d is 0.</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>DEC(size, d)</TableCell>
                            <TableCell>Equal to DECIMAL(size,d)</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>

                <Typography variant={"caption"}>
                    All the numeric data types may have an extra option: UNSIGNED or ZEROFILL. If you add the UNSIGNED
                    option, MySQL disallows negative values for the column.
                </Typography>
                <Typography variant={"caption"}>
                    If you add the ZEROFILL option, MySQL
                    automatically also adds the UNSIGNED attribute to the column.
                </Typography>

                <Typography>
                    <Bold>Date and Time data types</Bold>
                </Typography>

                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Data Types</TableCell>
                            <TableCell>Description</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>DATE</TableCell>
                            <TableCell>A date. Format: YYYY-MM-DD. The supported range is from '1000-01-01' to
                                '9999-12-31'</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>DATETIME(fsp)</TableCell>
                            <TableCell>A date and time combination. Format: YYYY-MM-DD hh:mm:ss. The supported range is
                                from
                                '1000-01-01 00:00:00' to '9999-12-31 23:59:59'. Adding DEFAULT and ON UPDATE in the
                                column
                                definition to get automatic initialization and updating to the current date and
                                time</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>TIMESTAMP(fsp)</TableCell>
                            <TableCell>A timestamp. TIMESTAMP values are stored as the number of seconds since the Unix
                                epoch
                                ('1970-01-01 00:00:00' UTC). Format: YYYY-MM-DD hh:mm:ss. The supported range is from
                                '1970-01-01 00:00:01' UTC to '2038-01-09 03:14:07' UTC. Automatic initialization and
                                updating
                                to the current date and time can be specified using DEFAULT CURRENT_TIMESTAMP and ON
                                UPDATE
                                CURRENT_TIMESTAMP in the column definition.</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>TIME(fsp)</TableCell>
                            <TableCell>A time. Format: hh:mm:ss. The supported range is from '-838:59:59' to
                                '838:59:59'</TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell>YEAR</TableCell>
                            <TableCell>A year in four-digit format. Values allowed in four-digit format: 1901 to 2155,
                                and 0000.
                                MySQL 8.0 does not support year in two-digit format.</TableCell>
                        </TableRow>
                    </TableBody>
                </Table>

                <Typography id={constraints.id} variant={'title'}>
                    {constraints.display}
                </Typography>

                <Typography variant={"caption"}>
                    Constraints can be specified when the table is created with the CREATE TABLE statement, or after the
                    table
                    is created with the ALTER TABLE statement.
                </Typography>
                <Typography variant={"caption"}>
                    SQL constraints are used to specify rules for the data in a table.
                </Typography>

                <Code>{constraintCode}</Code>

                <Typography variant={"caption"}>
                    <Bold>Commonly used constrains in SQL</Bold>

                    <ul>
                        <li><Bold>NOT NULL</Bold> Ensures that a column cannot have a NULL value</li>
                        <li><Bold>UNIQUE</Bold> Ensures that all values in a column are different</li>
                        <li><Bold>PRIMARY KEY</Bold> A combination of a NOT NULL and UNIQUE. Uniquely identifies each
                            row in
                            a table
                        </li>
                        <li><Bold>FOREIGN KEY</Bold> Uniquely identifies a row/record in another table</li>
                        <li><Bold>CHECK</Bold> Ensures that all values in a column satisfies a specific condition</li>
                        <li><Bold>DEFAULT</Bold> Sets a default value for a column when no value is specified</li>
                        <li><Bold>INDEX</Bold> Used to create and retrieve data from the database very quickly</li>
                    </ul>
                </Typography>

            </Fragment>
        )
    }
}

export default withStyles(styles)(DataTypes)
