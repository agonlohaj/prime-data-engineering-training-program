/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import {Bold, Italic} from "presentations/Label";
import SqlArithmeticOperatorsImg from 'assets/images/lecture4/sql_arithmetic_operators.png'
import Code from "presentations/Code";

const styles = ({ typography }) => ({
  root: {},
})

const selectStatement = `-- select all columns
SELECT * FROM employees;  -- (*) not always recommended to use

-- select first_name and last_name from employees table
SELECT first_name, last_name FROM employees;

-- select emp_no and salary from salaries table
SELECT emp_no, salary FROM salaries;
`

const selectWithAlias = `-- Alias example 1
SELECT first_name as FirstName, last_name as LastName FROM employees;

-- Alias example 2  
SELECT first_name as 'First Name', last_name as 'Last Name' FROM employees;
`

const selectDistinct = `SELECT DISTINCT last_name FROM employees;
SELECT COUNT(DISTINCT (first_name)) FROM employees;
`

const selectWhereEqual = `-- The one with equal operator
SELECT * FROM employees WHERE first_name = 'Elvis';

-- The one with not equal operator
SELECT * FROM employees WHERE first_name <> 'Elvis';
-- or
SELECT * FROM employees WHERE first_name != 'Elvis';

-- Counting with Where clause
SELECT count(*) FROM employees WHERE first_name = 'Elvis';
`

const selectWhereCombine = `SELECT * FROM employees WHERE first_name = 'Elvis' and gender = 'M';

SELECT count(*) FROM employees WHERE first_name = 'Elvis' and gender = 'M';

SELECT * FROM employees WHERE first_name = 'Elvis' or first_name = 'Chenye' and NOT (last_name = 'Velasco' and last_name='Luca');
`

const selectWhereIn = `SELECT * FROM employees WHERE first_name IN ('Elvis', 'Sumant','Berni', 'Lillian' );

SELECT count(*) FROM employees WHERE first_name IN ('Elvis', 'Sumant','Berni', 'Lillian' );

SELECT count(*) FROM employees WHERE first_name IN ('Elvis', 'Sumant','Berni', 'Lillian' ) 
AND last_name NOT IN ('Redmiles', 'Feldhoffer', 'Androutsos', 'Schaar');
`

const selectWhereIsNull = `SELECT * FROM titles WHERE to_date IS NULL;

SELECT * FROM titles WHERE to_date IS NOT NULL;`

const selectWhereComparison = `SELECT * FROM salaries WHERE salary > 66961;

SELECT count(*) FROM salaries WHERE salary > 66961;

SELECT count(*) FROM salaries WHERE salary < 66961;

SELECT count(*) FROM salaries WHERE salary > 66961 AND from_date > '1989-06-25';`

const selectWhereRange = `select count(*) from salaries where salary >= 66074 and salary <= 71046;

select count(*) from salaries where salary between 66074 and 71046;

select count(*) from employees where birth_date between '1954-05-01' and '1956-04-20';

select count(*) from employees where birth_date not between '1954-05-01' and '1956-04-20';
`

const selectWhereLike = `select * from employees where first_name like 'E%';

select * from employees where first_name like 'Elv%' and last_name like '_e%';

select * from employees where first_name like 'Elv%' and last_name like '_e%' 
and last_name not like '%n' ;`

const selectCase = `-- The one with CASE
select salary, CASE WHEN salary >= 45000 THEN 'Satisfied' ELSE 'Not Satisfied' END AS 'Remark' from salaries;

-- The one with IF
select salary, IF(salary >= 45000, 'Satisfied', 'Not Satisfied') AS 'Remark' from salaries;
-- Which means: IF salary >= 45000 is TRUE then return 'Satisfied' ELSE return 'Not Satisfied'`

const selectOrderByUsecases = `-- Use cases:
ORDER BY x ASC -- same as default
ORDER BY x DESC -- highest to lowest
ORDER BY lastname, firstname -- typical name sorting; using two columns
ORDER BY submit_date DESC -- latest first
ORDER BY submit_date DESC, id ASC -- latest first, but fully specifying order.`

const selectOrderByExamples = `select * from salaries order by from_date, salary desc;

select * from employees order by first_name;

select * from employees where first_name = 'Elvis' order by gender desc, birth_date desc, last_name;`

const selectLimit = `SELECT 
    first_name, last_name
FROM
    employees
ORDER BY first_name
LIMIT 10;`

const selectLimitBestPractice = `SELECT 
    first_name, last_name
FROM
    employees
ORDER BY first_name
LIMIT 2,1;

-- Alternatively:
SELECT 
    first_name, last_name
FROM
    employees
ORDER BY first_name, last_name 
LIMIT 1 OFFSET 2;`

const nestedQuery = `SELECT 
    *
FROM
    employees
WHERE
    first_name IN (SELECT DISTINCT
            first_name
        FROM
            employees
        WHERE
            first_name LIKE 'E%');`

const selectConcat = `SELECT first_name, last_name, CONCAT(first_name, last_name) as 'Name' FROM employees;
-- concat by an space char
SELECT first_name, last_name, CONCAT(first_name, ' ', last_name) as 'Name' FROM employees;

-- construct initials from first and last name
SELECT first_name, last_name, CONCAT(LEFT(first_name, 1), LEFT(last_name, 1)) as 'Initials' FROM employees;
-- Similarly, RIGHT() function can be used`

const arithmeticExpressions = `-- Let's lower the salary to a more realistic value
SELECT salary, salary * .01 as weekly FROM salaries;

-- The one with weekly, monthly and yearly
SELECT salary, 
salary * .01 as weekly,
salary * .01 * 4 as monthly,
salary * .01 * 52 as yearly  FROM salaries;

-- An example with divide and modulo operators
SELECT salary,
(salary + 200) * .01 as add_first,
salary *.01 / 7 as daily,
salary DIV 3 as div_op,
salary % 3 as mod_op FROM salaries;`

class SelectStatement extends React.Component {
  render() {
    const { classes, section } = this.props

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography>
          <Bold>The SELECT statement </Bold> is used to retrieve data from a database.
          <Code>
            {selectStatement}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SELECT with Alias (AS)
        </Typography>

        <Typography>
          When extracting reports, proper column naming is required, thus column aliases are used. SQL aliases are
          used to temporarily rename a table or a column. They are generally used to improve readability.
        </Typography>

        <Typography>
          <Code>
            {selectWithAlias}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          Select with DISTINCT
        </Typography>

        <Typography>
          The SELECT DISTINCT statement is used to return only distinct values from a table.
        </Typography>

        <Typography>
          <Code>
            {selectDistinct}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          Select with WHERE Clause
        </Typography>

        <Typography>
          The WHERE clause is used to tell MySQL the condition we want to use when filtering for data.
          <ul>
            <li>Equal and NOT Equal operators</li>
          </ul>
        </Typography>

        <Typography>
          <Code>
            {selectWhereEqual}
          </Code>
        </Typography>

        <Typography>
          <ul>
            <li>SQL Where clause with AND, OR and NOT</li>
            The WHERE clause uses AND, OR, and NOT operators to combine conditions.
          </ul>

        </Typography>

        <Typography>
          <Code>
            {selectWhereCombine}
          </Code>
        </Typography>

        <Typography>
          <ul>
            <li>SQL Where clause with IN and NOT IN operators</li>
            The IN operator allows you to specify multiple values in a WHERE clause.
          </ul>

        </Typography>

        <Typography>
          <Code>
            {selectWhereIn}
          </Code>
        </Typography>

        <Typography>
          <ul>
            <li>Dealing with Empty (NULL) values in Where clause</li>
            NULL represents an empty value in a caloumn of a table. NULL is not equal to "" (blank string) or 0
            (in case of integer).
          </ul>
        </Typography>

        <Typography>
          <Code>
            {selectWhereIsNull}
          </Code>
        </Typography>

        <Typography>
          <ul>
            <li>SQL Where clause with Greater-than and Less-than operators</li>
          </ul>
        </Typography>

        <Typography>
          <Code>
            {selectWhereComparison}
          </Code>
        </Typography>

        <Typography>
          <ul>
            <li>SQL Where with Range Queries - BETWEEN</li>
          </ul>
        </Typography>

        <Typography>
          {selectWhereRange}
        </Typography>

        <Typography>
          <ul>
            <li>SQL Where clause with LIKE and NOT LIKE</li>
            (_) in a LIKE clause is a wildcard that matches a single character.
            (%) in a LIKE clause is a wildcard that matches any character.
          </ul>
        </Typography>

        <Typography>
          <Code>
            {selectWhereLike}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          The SQL SELECT with CASE or IF
        </Typography>

        <Typography>
          CASE and IF operators are used to project a result based on the condition they match
        </Typography>

        <Typography>
          <Code>
            {selectCase}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SELECT with a ORDER BY clause
        </Typography>

        <Typography>
          ORDER BY X - X can be any datatype:
          <ul>
            <li>NULLs precede non-NULLs.</li>
            <li>The default is ASC (lowest to highest)</li>
            <li>Strings (VARCHAR, etc)</li>
            <li>ENUMs are ordered by the declaration order of its strings.</li>
          </ul>
        </Typography>

        <Typography>
          <Code>
            {selectOrderByUsecases}
          </Code>
        </Typography>

        <Typography>
          <Code>
            {selectOrderByExamples}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SELECT with a LIMIT clause
        </Typography>

        <Typography>
          LIMIT clause limits the number of records we want to retrieve from the database
        </Typography>

        <Typography>
          <Code>
            {selectLimit}
          </Code>
        </Typography>

        <Typography>
          <Bold>Best Practice: </Bold> Always use ORDER BY when using LIMIT; otherwise the rows you will get will be unpredictable.
        </Typography>

        <Typography>
          <Code>
            {selectLimitBestPractice}
          </Code>
        </Typography>

        <Typography>
          When a LIMIT clause contains two numbers, it is interpreted as LIMIT offset,count. So, in this example the
          query skips two records and returns one.
        </Typography>

        <Typography variant={'title'}>
          The SQL Sub-queries
        </Typography>

        <Typography>
          SQL Sub-queries are SELECT queries (aka nested query) declared within a valid SQL clause or used in
          conjunction with an SQL operator within WHERE clause.
        </Typography>

        <Typography>
          <Code>
            {nestedQuery}
          </Code>
        </Typography>

        <Typography>
          Concatenating column values with CONCAT() function.
        </Typography>

        <Typography>
          <Code>
            {selectConcat}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SQL Arithmetic Operators
        </Typography>

        <Typography>
          <img src={SqlArithmeticOperatorsImg} style={{maxWidth: 720}}/>
        </Typography>

        <Typography>
          Arithmetic operators can be applied to any numeric column.
        </Typography>

        <Typography>
          <Code>
            {arithmeticExpressions}
          </Code>
        </Typography>

      </Fragment>
    )
  }
}

export default withStyles(styles)(SelectStatement)
