/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import {Bold, Italic} from "presentations/Label";
import SqlJoinTypesImg from 'assets/images/lecture4/sql_join_types.png'
import Code from "presentations/Code";

const styles = ({ typography }) => ({
  root: {},
})

const joinAnatomy = `SELECT table1.column1,table1.column2,table2.column1
FROM table1 
JOIN table2
ON table1.matching_column = table2.matching_column;`

const innerJoin = `-- Selecting everything from two tables 
SELECT * FROM
employees emp
 JOIN 
 dept_manager dm
 ON emp.emp_no = dm.emp_no
 JOIN 
 departments dept
 ON dm.dept_no = dept.dept_no; 

-- Chaining more than two tables in a JOIN statement
SELECT * FROM
employees as emp
 JOIN 
 dept_manager as dm
 ON emp.emp_no = dm.emp_no
 JOIN 
 departments as dept
 ON dm.dept_no = dept.dept_no;

-- Selecting only desired columns from the tables
SELECT emp.emp_no, first_name, last_name, dept_name
FROM
employees emp
 JOIN 
 dept_manager dm
 ON emp.emp_no = dm.emp_no
 JOIN 
 departments dept
 ON dm.dept_no = dept.dept_no;


-- JOIN query with WHERE Clause
SELECT dept_name, emp.emp_no, first_name, last_name FROM
employees AS emp
 JOIN 
 dept_manager AS dm
 ON emp.emp_no = dm.emp_no
 JOIN 
 departments AS dept
 ON dm.dept_no = dept.dept_no
WHERE
 dm.to_date = '9999-01-01' 
 AND emp.gender = 'F'
order by dept_name;`

const naturalJoin = `SELECT 
    dept_name, first_name, last_name, title
FROM 
    employees AS emp
        NATURAL JOIN
    dept_manager AS dm
        NATURAL JOIN
    departments AS dept 
        NATURAL JOIN
    titles AS t;`

const rightJoin = `SELECT * FROM employees as emp
  RIGHT JOIN dept_manager as dm
  ON emp.emp_no = dm.emp_no;`

const fullJoin = `SELECT * FROM employees as emp
  FULL OUTER JOIN dept_manager as dm
  ON emp.emp_no = dm.emp_no;`

const leftJoin = `SELECT * FROM employees as emp
  LEFT JOIN dept_manager as dm
  ON emp.emp_no = dm.emp_no;`

const unionJoin = `SELECT 
    dept_name, emp.emp_no, first_name, last_name, 'Manager' as emp_type
FROM
    employees AS emp
        JOIN
    dept_manager AS dm ON emp.emp_no = dm.emp_no
        JOIN
    departments AS dept ON dm.dept_no = dept.dept_no
WHERE
    dm.to_date = '9999-01-01'
UNION
SELECT 
    dept_name, emp.emp_no, first_name, last_name, 'Staff' as emp_type
FROM
    employees AS emp
        JOIN
    dept_emp AS de ON emp.emp_no = de.emp_no
        JOIN
    departments AS dept ON de.dept_no = dept.dept_no
        JOIN
    titles AS t ON t.emp_no = emp.emp_no
WHERE
    de.to_date = '9999-01-01'
        AND t.to_date = '9999-01-01'
ORDER BY emp_type, dept_name, last_name;`

const selfJoin = `SELECT column_name(s)
FROM employee emp1, employee emp2
WHERE <some_condition>;`

const viewAnatomy = `CREATE VIEW view_name AS
SELECT column1, column2
FROM table_name
WHERE condition;`

const viewCreation = `-- Creates a view with a SELECT query

CREATE VIEW order_data AS
 SELECT 
    employees.first_name as employee_first_name, employees.last_name as employee_last_name, 
    customers.first_name as customer_first_name, customers.last_name as customer_last_name,
    shippers.company as shipper_name, products.product_code, products.product_name, 
    orders_status.status_name as order_status, order_details_status.status_name as order_detail_status,
    orders_tax_status.tax_status_name
FROM
    orders
        JOIN
    order_details ON orders.id = order_details.id
        JOIN
    customers ON orders.customer_id = customers.id
        JOIN
    employees ON orders.employee_id = employees.id
        JOIN
    products ON order_details.product_id = products.id
        JOIN
    orders_tax_status ON ifnull(orders.tax_status_id, 1) = orders_tax_status.id
        JOIN
    orders_status ON orders.status_id = orders_status.id
        JOIN
    order_details_status ON order_details.status_id = order_details_status.id
        JOIN
    shippers ON orders.shipper_id = shippers.id;

-- Querying database using a table VIEW
select * from order_data; 

-- Drop the view
DROP VIEW order_data;`


class Joins extends React.Component {
  render() {
    const { classes, section } = this.props
    const viewStatement = section.children[0]

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography>
          Up to this point, we’ve only been working with one table at a time. The real power of SQL, however, comes
          from working with data from multiple tables at once.
        </Typography>

        <Typography>
          A JOIN statement is used to combine rows from two or more tables into a single result set.
        </Typography>

        <Typography variant={'title'}>
          The Anatomy of a JOIN statement
        </Typography>

        <Typography>
          <Code>
            {joinAnatomy}
          </Code>
          <ul>
            <li>The ON keyword allows you to specify the join criteria.</li>
            <li>Join criteria normally uses the equals operator, but other operators may be used</li>
            <li>The INNER and OUTER key words are optional, and may be omitted.</li>
          </ul>
        </Typography>

        <Typography variant={'title'}>
          SQL JOIN Types
        </Typography>

        <Typography>
          <img src={SqlJoinTypesImg} style={{maxWidth: 720}} />
        </Typography>

        <Typography>
          Because every SQL tutorial has this picture
        </Typography>

        <Typography variant={'title'}>
          SQL INNER JOIN
        </Typography>

        <Typography>
          (INNER) JOIN: Returns records that have matching values in both tables
        </Typography>

        <Typography>
          <Code>
            {innerJoin}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SQL NATURAL Join
        </Typography>

        <Typography>
          It will join tables where columns of the same name are equal.
        </Typography>

        <Typography>
          <Code>
            {naturalJoin}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SQL RIGHT JOIN
        </Typography>

        <Typography>
          It all records from the right table, and the matched records from the left table. The result is NULL from
          the left side, when there is no match.
        </Typography>

        <Typography>
          <Code>
            {rightJoin}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SQL FULL (OUTER) JOIN
        </Typography>

        <Typography>
          It returns all records when there is a match in either left or right table records.
        </Typography>

        <Typography>
          <Code>
            {fullJoin}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SQL LEFT JOIN
        </Typography>

        <Typography>
          It returns all records from the left table, and the matched records from the right table. The result is
          NULL from the right side, if there is no match.
        </Typography>

        <Typography>
          <Code>
            {leftJoin}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SQL UNION JOIN
        </Typography>

        <Typography>
          It will combines rows from one or more tables using SELECT statement.
          <ul>
            <li>Each SELECT statement within UNION must have the same number of columns</li>
            <li>The columns must also have similar data types</li>
            <li>The columns in each SELECT statement must also be in the same order</li>
          </ul>
        </Typography>

        <Typography>
          <Code>
            {unionJoin}
          </Code>
        </Typography>

        <Typography variant={'title'}>
          SQL Self JOIN
        </Typography>

        <Typography>
          A self JOIN is a regular join, but the table is joined with itself.
        </Typography>

        <Typography>
          <Code>
            {selfJoin}
          </Code>
        </Typography>

        <Typography id={viewStatement.id} variant={'subHeading'}>
          {viewStatement.display}
        </Typography>

        <Typography>
          In SQL, a view is a virtual table based on the result-set of an SQL statement.
        </Typography>

        <Typography>
          A view contains rows and columns, just like a real table. The fields in a view are fields from one or more
          real tables in the database.
        </Typography>

        <Typography>
          You can add SQL functions, WHERE, and JOIN statements to a view and present the data as if the data were
          coming from one single table.
        </Typography>

        <Typography variant={'title'}>
          The Anatomy of VIEW Statement
        </Typography>

        <Typography>
          <Code>
            {viewAnatomy}
          </Code>
        </Typography>

        <Typography>
          The following SQL statement creates a view that shows:
          <ul>
            <li>Employee first name, last name, customer first name and last name</li>
            <li>Shipper name, product code, product name, order status name, order detail status name</li>
            <li>Tax Status Name</li>
          </ul>
        </Typography>

        <Typography>
          <Code>
            {viewCreation}
          </Code>
        </Typography>

        <Typography>
          The database engine generates the result every time a user queries a view.
        </Typography>

        <Typography>
          <Code>CREATE OR REPLACE VIEW is used to update an existing table view. If it doesn't exist it will create the view.</Code>
        </Typography>

        <Typography variant={'title'}>
          Materialized Views
        </Typography>

        <Typography>
          Materialized Views are similar to regular database views in the fact they are generated from physical
          database tables via a SQL select statement. Currently not natively supported in MySQL, but worth having
          a clue they exist.
        </Typography>

        <Typography>
          Characteristics:
          <ul>
            <li>Key difference is the result set is actually persisted to disk.</li>
            <li>Result set is calculated ahead of time and is periodically refreshed.</li>
            <li>Materialized Views are often used for ‘heavy weight’ queries. ie a query which is expensive to execute.</li>
            <li>Ideal for data which needs to be read, which does not change often.</li>
          </ul>
          Materialized Views can be refreshed in different kinds:
          <ul>
            <li>never (only once in the beginning, for static data only)</li>
            <li>on demand (for example once a day, for example after nightly load)</li>
            <li>immediately (after each statement)</li>
          </ul>
        </Typography>

        <Typography>
          <Bold>Exercise 1: </Bold> Create a table VIEW which returns employees of department Development.
        </Typography>

      </Fragment>
    )
  }
}

export default withStyles(styles)(Joins)
