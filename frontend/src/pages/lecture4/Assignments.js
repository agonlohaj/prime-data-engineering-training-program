/**
 * Created by Agon Lohaj on 27/08/2020.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import Code from "presentations/Code";
import SimpleLink from "presentations/rows/SimpleLink";
import faker from 'faker'
import withTests from 'middleware/withTests'
import LoadingIndicator from "presentations/LoadingIndicator";
import ErrorBox from "presentations/ErrorBox";
import Chart from "presentations/Chart";
import {Bold, Highlighted} from "presentations/Label";
import { Button } from "@material-ui/core";
import Exercise from "presentations/Exercise";

const styles = ({ size, palette, typography }) => ({
  root: {},
  graphs: {
    display: 'flex',
    flexFlow: 'row wrap',
    alignItems: 'flex-start',
    alignContent: 'flex-start',
    width: '100%'
  },
  title: {
    zIndex: 10,
  },
  card: {
    backgroundColor: 'white',
    width: `calc(32% - ${size.spacing * 2}px)`,
    margin: size.spacing,
    height: 420,
    padding: 8,
    display: 'flex',
    flexFlow: 'column wrap',
    position: 'relative',
    alignItems: 'flex-start'
  },
  graph: {
    display: 'flex',
    flex: 1,
    width: '100%',
    height: 'auto'
  },
  copy: {
    position: 'absolute',
    bottom: size.spacing,
    right: size.spacing,
    color: palette.leadColor,
    zIndex: 500
  }
})

const graphRoutes = `GET            /api/assignments/lecture4/assignment1                                    @io.training.api.controllers.AssignmentLectureTenController.assignment1(request: Request)
GET            /api/assignments/lecture4/assignment2                                    @io.training.api.controllers.AssignmentLectureTenController.assignment2(request: Request)
GET            /api/assignments/lecture4/assignment3                                    @io.training.api.controllers.AssignmentLectureTenController.assignment3(request: Request)
GET            /api/assignments/lecture4/assignment4                                    @io.training.api.controllers.AssignmentLectureTenController.assignment4(request: Request)
GET            /api/assignments/lecture4/assignment5                                    @io.training.api.controllers.AssignmentLectureTenController.assignment5(request: Request)
GET            /api/assignments/lecture4/assignment6                                    @io.training.api.controllers.AssignmentLectureTenController.assignment6(request: Request)`
const knightTourRoutes = `GET            /api/assignments/lecture4/knightTour           @io.training.api.controllers.AssignmentLectureTenController.knightTour(request: Request)`


const Card = (props) => {
  const { call, request, enabled, title, classes,  ...other } = props
  const { error, isLoading, response, runTests } = withTests([request], (input) => {
    const result = {
      endpoint: request.endpoint,
      options: {
        method: request.method,
      }
    }
    if (request.method === 'GET') {
      return result;
    }
    return {...result, options: {
        method: request.method,
        body: JSON.stringify(request.body),
      }}
  }, (input, output) => {
    return true
  }, (input, output) => {
    return output
  }, (input, error) => {
    return error.status === request.code
  }, enabled)

  const status = error ? error.status : !!response ? 200 : 0
  const isSet = status === request.code

  let display = 'Success'
  if (isLoading) {
    display = 'Loading'
  } else if (status !== request.code) {
    display = 'Not Correct, Retry?'
  }

  const onCopy = (event) => {
    event.preventDefault()
    event.stopPropagation()
    navigator.clipboard.writeText(JSON.stringify(request.sample, null, " "))
  }

  return <div {...other}>
    <Typography variant={'title'} className={classes.titleClass}>{title}</Typography>
    <LoadingIndicator show={isLoading} />
    {!isSet && <ErrorBox message={error && (error.message || 'Failed to contact the server! Make sure its on?')} onRetryClicked={onCopy} />}
    {!isSet && <Button className={classes.copy} onClick={onCopy}>Copy Response</Button>}
    <Chart className={classes.graphClass} options={response} />
  </div>
}

const exercise4 = `id,date
1,2022-02-01
1,2022-02-02
1,2022-02-03
1,2022-02-05
1,2022-02-06
2,2022-02-01
2,2022-02-02

Should output the 'name', 'contact name' and '3' of id = 1, because id = 1 ordered 3 days in a row, then two, while id = 2 
ordered only two days in a row`
const exercise5 = `id,date
1,2022-02-01
1,2022-02-02
1,2022-02-03
1,2022-02-05
1,2022-02-06

Should give 1,1 (CustomerID = 1, days = 1), since on 2022-02-04, no order was made; hint: use "sequence" and "explode"`
const intro = `In the following assignments, you will have to use these tables:

customers        ->  \`CustomerID\` STRING,    \`CustomerName\` STRING,  \`ContactName\` STRING,   \`Address\` STRING,
                     \`City\` STRING,          \`PostalCode\` STRING,    \`Country\` STRING

employees        ->  \`EmployeeID\` STRING,    \`LastName\` STRING,      \`FirstName\` STRING,     \`BirthDate\` STRING,
                     \`Photo\` STRING,         \`Notes\` STRING

orders           ->  \`OrderID\` STRING,       \`CustomerID\` STRING,    \`EmployeeID\` STRING,    \`OrderDate\` STRING,
                     \`ShipperID\` STRING

order_details    ->  \`OrderDetailID\` STRING, \`OrderID\` STRING,       \`ProductID\` STRING,     \`Quantity\` STRING

products         ->  \`ProductID\` STRING,     \`ProductName\` STRING,   \`SupplierID\` STRING,    \`CategoryID\` STRING,
                     \`Unit\` STRING,\`Price\` STRING

You can query the table through the name, as: 'select * from employees'`
const Assignments = (props) => {
  const { classes, section } = props
  const sqlExpressions = section.children[0]
  const algorithm = section.children[1]

  return (
    <Fragment>
      <Typography variant={'heading'}>
        {section.display}
        <Divider />
      </Typography>

      <Typography>
        Playground for exercises: use this space to test out queries with tables provided as below.
      </Typography>
      <Exercise url={'/assignments/lecture4/sqlPlayground'} />

      <Typography variant='title' id={sqlExpressions.id}>
        {sqlExpressions.display} Assignments
      </Typography>

      <Typography>Following assignments have to be implemented in SQL.</Typography>
      <Typography><Code>{intro}</Code></Typography>
      <Typography>
        <Bold>Assignment 1</Bold>: Show full name (`FirstName` and `LastName`) of the eldest employee
      </Typography>
      <Exercise url={'/assignments/lecture4/assignment1'} />

      <Typography>
        <Bold>Assignment 2</Bold>: Find the categories that have sales of over 50.000, excluding sales supplier `SupplierId` = 1
      </Typography>
      <Exercise url={'/assignments/lecture4/assignment2'} />

      <Typography>
        <Bold>Assignment 3</Bold>: Find the top 10 customers (`CustomerName`, `Address` and `City`) who have ordered the most
      </Typography>
      <Exercise url={'/assignments/lecture4/assignment3'} />

      <Typography>
        <Bold>Assignment 4</Bold>: Find the customers (`CustomerName`, `ContactName`, `NrOfDays`) that have ordered the most in consecutive days
      </Typography>
      <Typography>
        <Code>{exercise4}</Code>
      </Typography>
      <Exercise url={'/assignments/lecture4/assignment4'} />

      <Typography>
        <Bold>Assignment 5</Bold>: Find all the days that the customer did not do an order from the first date to the
        last that he purchased (`CustomerID` and `days`). For example:
      </Typography>
      <Typography>
        <Code>{exercise5}</Code>
      </Typography>
      <Exercise url={'/assignments/lecture4/assignment5'} />

      <Typography variant='title' id={algorithm.id}>
        {algorithm.display} Bonus!
      </Typography>
      <Typography variant='p'>
        Title: "Solve the Knights Tour Problem"<br/>
        Description: "Using Backtracking algorithm I am going to solve the Knights Tour Problem!"<br/>
        Here is the Explanation of the problem and a solution: <SimpleLink href="https://www.geeksforgeeks.org/the-knights-tour-problem-backtracking-1/">Knights Tour Problem</SimpleLink><br/>
      </Typography>
      <Typography variant='p'>
        Implement the solution at:
        <Code>
          {knightTourRoutes}
        </Code>
        <Typography fontStyle="italic">
          Use static data (your own) for Knights Tour problem.
        </Typography>
      </Typography>
    </Fragment>
  )
}

export default withStyles(styles)(Assignments)
