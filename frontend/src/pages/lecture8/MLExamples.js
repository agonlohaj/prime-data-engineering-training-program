/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import Code from "presentations/Code";


const styles = ({ typography }) => ({
  root: {},
})

const fullCode = `import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.feature.{HashingTF, Tokenizer}
import org.apache.spark.ml.linalg.Vector
import org.apache.spark.sql.Row


// Prepare training documents from a list of (id, text, label) tuples.
val training = spark.createDataFrame(Seq(
  (0L, "a b c d e spark", 1.0),
  (1L, "b d", 0.0),
  (2L, "spark f g h", 1.0),
  (3L, "hadoop mapreduce", 0.0)
)).toDF("id", "text", "label")
// training.show(10)
+---+----------------+-----+
| id|            text|label|
+---+----------------+-----+
|  0| a b c d e spark|  1.0|
|  1|             b d|  0.0|
|  2|     spark f g h|  1.0|
|  3|hadoop mapreduce|  0.0|
+---+----------------+-----+


// Configure an ML pipeline, which consists of three stages: tokenizer, hashingTF, and lr.
val tokenizer = new Tokenizer().setInputCol("text").setOutputCol("words")
// tokenizer.transform(training)
+---+----------------+-----+--------------------+
| id|            text|label|               words|
+---+----------------+-----+--------------------+
|  0| a b c d e spark|  1.0|[a, b, c, d, e, s...|
|  1|             b d|  0.0|              [b, d]|
|  2|     spark f g h|  1.0|    [spark, f, g, h]|
|  3|hadoop mapreduce|  0.0| [hadoop, mapreduce]|
+---+----------------+-----+--------------------+
  
  
val hashingTF = new HashingTF().setNumFeatures(1000).setInputCol(tokenizer.getOutputCol).setOutputCol("features")
// hashingTF.transform(modified training)
+---+----------------+-----+----------------------+----------------------------------------------------------+
|id |text            |label|words                 |features                                                  |
+---+----------------+-----+----------------------+----------------------------------------------------------+
|0  |a b c d e spark |1.0  |[a, b, c, d, e, spark]|(1000,[165,286,467,550,768,890],[1.0,1.0,1.0,1.0,1.0,1.0])|
|1  |b d             |0.0  |[b, d]                |(1000,[165,890],[1.0,1.0])                                |
|2  |spark f g h     |1.0  |[spark, f, g, h]      |(1000,[286,486,494,979],[1.0,1.0,1.0,1.0])                |
|3  |hadoop mapreduce|0.0  |[hadoop, mapreduce]   |(1000,[585,750],[1.0,1.0])                                |
+---+----------------+-----+----------------------+----------------------------------------------------------+
  
  
val lr = new LogisticRegression().setMaxIter(10).setRegParam(0.001)
val pipeline = new Pipeline().setStages(Array(tokenizer, hashingTF, lr))

// Fit the pipeline to training documents.
val model = pipeline.fit(training)

// Now we can optionally save the fitted pipeline to disk
model.write.overwrite().save("/tmp/spark-logistic-regression-model")

// We can also save this unfit pipeline to disk
pipeline.write.overwrite().save("/tmp/unfit-lr-model")

// And load it back in during production
val sameModel = PipelineModel.load("/tmp/spark-logistic-regression-model")

// Prepare test documents, which are unlabeled (id, text) tuples.
val test = spark.createDataFrame(Seq(
  (4L, "spark i j k"),
  (5L, "l m n"),
  (6L, "spark hadoop spark"),
  (7L, "apache hadoop")
)).toDF("id", "text")


// Make predictions on test documents.
model.transform(test).select("id", "text", "probability", "prediction").show(10)
+---+----------------+-----+
| id|            text|label|
+---+----------------+-----+
|  0| a b c d e spark|  1.0|
|  1|             b d|  0.0|
|  2|     spark f g h|  1.0|
|  3|hadoop mapreduce|  0.0|
+---+----------------+-----+
  `

class MLExamples extends React.Component {
  render() {
    const { classes, section } = this.props

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography>
          Following is a full detailed example of a Logistic Regression from Spark documentation.
        </Typography>

        <Typography><Code>{fullCode}</Code></Typography>

        <Typography>Find full example code at "examples/src/main/scala/org/apache/spark/examples/ml/PipelineExample.scala"
          in the Spark repo.
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(MLExamples)
