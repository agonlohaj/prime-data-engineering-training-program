/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import {Bold, Italic} from "presentations/Label";
import Code from "presentations/Code";

const styles = ({ typography }) => ({
  root: {},
})

const RMSE = `import org.apache.spark.ml.evaluation.RegressionEvaluator
val regressionEvaluator = new RegressionEvaluator()
 .setPredictionCol("prediction")
 .setLabelCol("price")
 .setMetricName("rmse")
val rmse = regressionEvaluator.evaluate(predDF)
println(f"RMSE is $rmse%.1f")

Produces the following output: 220.6
`
const r2code = `val r2 = regressionEvaluator.setMetricName("r2").evaluate(predDF)
println(s"R2 is $r2")
`
const saveLoad = `val pipelinePath = "/tmp/lr-pipeline-model"
pipelineModel.write.overwrite().save(pipelinePath)`

const pipelineLoad = `import org.apache.spark.ml.PipelineModel
val savedPipelineModel = PipelineModel.load(pipelinePath)`

class MLEvaluation extends React.Component {
  render() {
    const { classes, section } = this.props
    const rmse = section.children[0]
    const r2 = section.children[1]
    const savingLoading = section.children[2]


    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography>
          Now that we have built a model, we need to evaluate how well it performs. In
          spark.ml there are classification, regression, clustering, and ranking evaluators
          (introduced in Spark 3.0).
        </Typography>

        <Typography variant={'title'} id={rmse.id}>
          {rmse.display}
        </Typography>

        <Typography>
          RMSE is a metric that ranges from zero to infinity. The closer it is to zero, the
          better. Let’s evaluate our model using RMSE:
        </Typography>

        <Typography>
          <Code>{RMSE}</Code>
        </Typography>

        <Typography>
          So how do we know if 220.6 is a good value for the
          RMSE? There are various ways to interpret this value, one of which is to build a sim‐
          ple baseline model and compute its RMSE to compare against. A common baseline
          model for regression tasks is to compute the average value of the label on the training
          set ȳ (pronounced y-bar), then predict ȳ for every record in the test data set and compute the resulting RMSE.
          If you try this, you will see that our baseline model has an RMSE of 240.7, so we beat our baseline. If you
          don’t beat the baseline, then something probably went wrong in your model building process.
        </Typography>

        <Typography variant={'title'} id={r2.id}>
          {r2.display}
        </Typography>

        <Typography>
          Despite the name R^2 containing “squared,” R^2 values range from negative infinity to 1. The nice thing about
          using R^2 is that you don’t necessarily need to define a baseline model to compare against.
        </Typography>

        <Typography>
          If we want to change our regression evaluator to use R^2, instead of redefining the
          regression evaluator, we can set the metric name using the setter property:
        </Typography>

        <Typography><Code>{r2code}</Code></Typography>

        <Typography>
          Our R^2 is positive, but it’s very close to 0. One of the reasons why our model is not
          performing too well is because our label, price, appears to be log-normally distributed. If a distribution
          is log-normal, it means that if we take the logarithm of the value, the result looks like a normal
          distribution. Price is often log-normally distributed.
        </Typography>

        <Typography variant={'title'} id={savingLoading.id}>
          {savingLoading.display}
        </Typography>

        <Typography>
          Now that we have built and evaluated a model, let’s save it to persistent storage for
          reuse later.
        </Typography>

        <Typography>
          Saving models is very similar to writing DataFrames—the API is
          model.write().save(path). You can optionally provide the overwrite() command
          to overwrite any data contained in that path:
        </Typography>

        <Typography>
          <Code>{saveLoad}</Code>
        </Typography>

        <Typography>
          When you load your saved models, you need to specify the type of model you are
          loading back in (e.g., was it a LinearRegressionModel or a LogisticRegressionMo
          del?). For this reason, we recommend you always put your transformers/estimators
          into a Pipeline, so that for all your models you load a PipelineModel and only need
          to change the file path to the model:
        </Typography>

        <Typography>
          <Code>{pipelineLoad}</Code>
        </Typography>

        <Typography>
          After loading, you can apply it to new data points.
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(MLEvaluation)
