/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import AWSGlobalInfrastructureMapImg from "assets/images/lecture5/global_infrastructure_map.png";
import AWSTableOfRegionsImg from "assets/images/lecture5/aws_table_of_regions.png";
import AWSAvailabilityZones from "assets/images/lecture5/aws_availability_zones.png";

const styles = ({ typography }) => ({
  root: {},
})


class AWSCloudProvider extends React.Component {
  render() {
    const { classes, section } = this.props
    const global_infrastructure_map = section.children[0]

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography variant={"citation"}>
          Amazon Web Services (AWS) is the world’s most comprehensive and broadly adopted cloud platform, offering
          over 165 fully featured services from data centers globally. Millions of customers —including the
          fastest-growing startups, largest enterprises, and leading government agencies—trust AWS to power
          their infrastructure, become more agile, and lower costs.
        </Typography>

        <Typography>
          The AWS cloud platform offers over 165 fully featured services, including over 40 services that aren’t
          available anywhere else.
        </Typography>

        <Typography id={global_infrastructure_map.id} variant={'title'}>
          {global_infrastructure_map.display}
        </Typography>

        <Typography>
          <img src={AWSGlobalInfrastructureMapImg} style={{maxWidth: 720}} />
        </Typography>

        <Typography>
          Amazon cloud services are hosted in multiple locations world-wide. These locations are composed of
          Regions and Availability Zones. Each Region is a separate geographic area.
        </Typography>

        <Typography>
          Each Region has multiple, isolated locations known as Availability Zones. Amazon EC2 provides you the
          ability to place resources, such as instances, and data in multiple locations. Resources aren't replicated
          across Regions unless you do so specifically.
        </Typography>

        <Typography>
          The following table lists the Regions provided by an AWS account:
        </Typography>

        <Typography>
          <img src={AWSTableOfRegionsImg} style={{maxWidth: 350}}/>
        </Typography>

        <Typography>
          <img src={AWSAvailabilityZones} style={{maxWidth: 720}}/>
        </Typography>



      </Fragment>
    )
  }
}

export default withStyles(styles)(AWSCloudProvider)
