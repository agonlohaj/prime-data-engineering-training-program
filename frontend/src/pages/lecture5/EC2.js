/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import {Bold} from "presentations/Label";
import EC2InstanceComponentsImg from 'assets/images/lecture5/ec2_instance_components.png'
import AMIInstanceTypesImg from "assets/images/lecture5/ami_instance_types.png";
import EC2StorageImg from "assets/images/lecture5/ec2_storage.png";
import EC2SecurityGroupImg from "assets/images/lecture5/security_group.png";
import VPCCompomentsImg from "assets/images/lecture5/vpc_components.png";

const styles = ({ typography }) => ({
  root: {},
})


class EC2 extends React.Component {
  render() {
    const { classes, section } = this.props
    const featureOfEC2 = section.children[0]
    const amiInstances = section.children[1]
    const instanceTypes = section.children[2]
    const instancePurchasingOptions = section.children[3]
    const storage = section.children[4]
    const securityGroup = section.children[5]
    const vpc = section.children[6]

    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>

        <Typography>
          Amazon Elastic Compute Cloud (Amazon EC2) provides scalable computing capacity in the Amazon Web Services (AWS) cloud.
        </Typography>

        <Typography>
          <Bold>
            You can use Amazon EC2 to launch as many or as few virtual servers as you need, configure security
            and networking, and manage storage.
          </Bold>
        </Typography>

        <Typography>
          <img src={EC2InstanceComponentsImg} style={{maxWidth: 500}}/>
        </Typography>

        <Typography id={featureOfEC2.id} variant={'title'}>
          {featureOfEC2.display}
        </Typography>

        <Typography>
          Amazon EC2 provides the following features:
          <ul>
            <li>
              Virtual computing environments, known as instances
            </li>
            <li>
              Preconfigured templates for your instances, known as Amazon Machine Images (AMIs), that package the bits
              you need for your server (including the operating system and additional software)
            </li>
            <li>
              Various configurations of CPU, memory, storage, and networking capacity for your instances, known as
              instance types
            </li>
            <li>
              Secure login information for your instances using key pairs (AWS stores the public key, and you store
              the private key in a secure place)
            </li>
            <li>
              Storage volumes for temporary data that's deleted when you stop or terminate your instance, known as
              instance store volumes
            </li>
            <li>
              Persistent storage volumes for your data using Amazon Elastic Block Store (Amazon EBS), known as Amazon
              EBS volumes
            </li>
            <li>
              Multiple physical locations for your resources, such as instances and Amazon EBS volumes, known as
              Regions and Availability Zones
            </li>
            <li>
              A firewall that enables you to specify the protocols, ports, and source IP ranges that can reach
              your instances using security groups
            </li>
            <li>
              Static IPv4 addresses for dynamic cloud computing, known as Elastic IP addresses
            </li>
            <li>
              Metadata, known as tags, that you can create and assign to your Amazon EC2 resources
            </li>
            <li>
              Virtual networks you can create that are logically isolated from the rest of the AWS cloud, and that
              you can optionally connect to your own network, known as virtual private clouds (VPCs)
            </li>
          </ul>
        </Typography>

        <Typography id={amiInstances.id} variant={'title'}>
          {amiInstances.display}
        </Typography>

        <Typography>
          An instance is a virtual server in the cloud. Its configuration at launch is a copy of the AMI that you
          specified when you launched the instance.
        </Typography>

        <Typography>
          An Amazon Machine Image (AMI) is a template that contains a software configuration (for example, an
          operating system, an application server, and applications).
        </Typography>

        <Typography>
          From an AMI, you launch an instance, which is a copy of the AMI running as a virtual server in the cloud.
           You can launch multiple instances of an AMI, as shown in the following figure.
        </Typography>

        <Typography>
          <img src={AMIInstanceTypesImg} />
        </Typography>

        <Typography>
          AMI Types can be Linux or Windows based images.
        </Typography>

        <Typography id={instanceTypes.id} variant={'title'}>
          {instanceTypes.display}
        </Typography>

        <Typography>
          When you launch an instance, the instance type that you specify determines the hardware of the host
          computer used for your instance.
        </Typography>

        <Typography>
          Each instance type offers different compute, memory, and storage capabilities and are grouped in instance
          families based on these capabilities.
        </Typography>

        <Typography>
          AWS EC2 provides a wide range of instance types, you can choose the type based on the workload of your
          application.
        </Typography>

        <Typography id={instancePurchasingOptions.id} variant={'title'}>
          {instancePurchasingOptions.display}
        </Typography>

        <Typography>
          Amazon EC2 provides the following purchasing options to enable you to optimize your costs based on your needs:
          <ul>
            <li>
              On-Demand Instances – Pay, by the second, for the instances that you launch.
            </li>
            <li>
              Reserved Instances – Purchase, at a significant discount, instances that are always available, for a
              term from one to three years.
            </li>
            <li>
              Scheduled Instances – Purchase instances that are always available on the specified recurring schedule,
              for a one-year term.
            </li>
            <li>
              Spot Instances – Request unused EC2 instances, which can lower your Amazon EC2 costs significantly.
            </li>
            <li>
              Dedicated Hosts – Pay for a physical host that is fully dedicated to running your instances, and bring
              your existing per-socket, per-core, or per-VM software licenses to reduce costs.
            </li>
            <li>
              Dedicated Instances – Pay, by the hour, for instances that run on single-tenant hardware.
            </li>
            <li>
              Capacity Reservations – Reserve capacity for your EC2 instances in a specific Availability Zone for
              any duration.
            </li>
          </ul>
        </Typography>

        <Typography id={storage.id} variant={'title'}>
          {storage.display}
        </Typography>

        <Typography>
          Amazon EC2 provides you with flexible, cost effective, and easy-to-use data storage options for your
          instances. It refers to the hard-disk capacity.
        </Typography>

        <Typography>
          These storage options include the following:
          <ul>
            <li>Amazon Elastic Block Store (EBS)</li>
            <li>Amazon EC2 Instance Store</li>
            <li>Amazon Elastic File System (Amazon EFS)</li>
            <li>Amazon Simple Storage Service (Amazon S3)</li>
          </ul>
          <img src={EC2StorageImg} />
        </Typography>

        <Typography>
          <ul>
            <li><Bold>Amazon EBS</Bold></li>
          </ul>
          Amazon EBS provides durable, block-level storage volumes that you can attach to a running instance. You can
          use Amazon EBS as a primary storage device for data that requires frequent and granular updates. For example,
          Amazon EBS is the recommended storage option when you run a database on an instance.
        </Typography>

        <Typography>
          <ul>
            <li><Bold>Amazon EC2 Instance Store</Bold></li>
          </ul>
          Many instances can access storage from disks that are physically attached to the host computer. This disk
          storage is referred to as instance store. Instance store provides temporary block-level storage for
          instances. The data on an instance store volume persists only during the life of the associated instance; if
          you stop or terminate an instance, any data on instance store volumes is lost. For more information, see
          Amazon EC2 Instance Store.
        </Typography>

        <Typography>
          <ul>
            <li><Bold>Amazon EFS File System</Bold></li>
          </ul>
          Amazon EFS provides scalable file storage for use with Amazon EC2. You can create an EFS file system and
          configure your instances to mount the file system. You can use an EFS file system as a common data source for
          workloads and applications running on multiple instances.
        </Typography>

        <Typography id={securityGroup.id} variant={'title'}>
          {securityGroup.display}
        </Typography>

        <Typography>
          The rules of a security group control the inbound traffic that's allowed to reach the instances that are
          associated with the security group and the outbound traffic that's allowed to leave them.
        </Typography>

        <Typography>
          <img src={EC2SecurityGroupImg} style={{maxWidth: 420}}/>
        </Typography>

        <Typography id={vpc.id} variant={'title'}>
          {vpc.display}
        </Typography>

        <Typography>
          A virtual private cloud (VPC) is a virtual network dedicated to your AWS account. It is logically isolated
          from other virtual networks in the AWS Cloud. You can launch your AWS resources, such as Amazon EC2 instances,
          into your VPC.
        </Typography>

        <Typography>
          Within VPC you can specify:
          <ul>
            <li>IP address range for the VPC,</li>
            <li>Subnets,</li>
            <li>Associate Security Groups,</li>
            <li>Configure Route Tables.</li>
          </ul>
        </Typography>

        <Typography>
          <img src={VPCCompomentsImg} />
        </Typography>

        <Typography>
          Components of a VPC:
          <ul>
            <li>Subnets</li>
            <li>Security Groups</li>
            <li>Network ACLs</li>
            <li>Internet Gateways</li>
            <li>Egress Only Internet Gateways</li>
            <li>Route Tables</li>
            <li>Network Interfaces</li>
            <li>Peering Connections</li>
            <li>Endpoints</li>
          </ul>
        </Typography>


      </Fragment>
    )
  }
}

export default withStyles(styles)(EC2)
