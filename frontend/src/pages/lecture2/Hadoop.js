/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, { Fragment } from "react";
import HadoopArchitectureImage from 'assets/images/lecture2/hadoop_architecture.png'
import { Bold } from 'presentations/Label'
import Code from 'presentations/Code'

const styles = ({ typography }) => ({
  root: {},
})

const fileSystems =`Hadoop—these have the hdfs:// file prefix in their name.
For example hdfs://masterServer:9000/folder/file.txt is a valid file name.
S3—Amazon S3.  The file prefix is s3n://
file—file:// causes Hadoop to use any other distributed file system.`
class Hadoop extends React.Component {
  render() {
    const { classes, section } = this.props
    const architecture = section.children[0]
    const storage = section.children[1]
    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>
        <Typography>
          The Apache Hadoop software library is a framework that allows for the distributed processing of large data sets across clusters of computers using simple programming models. It is designed to scale up from single servers to thousands of machines, each offering local computation and storage. Rather than rely on hardware to deliver high-availability, the library itself is designed to detect and handle failures at the application layer, so delivering a highly-available service on top of a cluster of computers, each of which may be prone to failures.
        </Typography>
        <Typography>
          Hadoop is designed to be fault tolerant. You tell it how many times to replication data. Then when a datanode crashes data is not lost.
        </Typography>
        <Typography>
          Hadoop uses a master-slave architecture. The basic premise of its design is to Bring the computing to the data instead of the data to the computing. That makes sense. It stores data files that are too large to fit on one server across multiple servers. Then when is does Map and Reduce operations it further divides those and lets each server in the node do the computing. So each node is a computer and not just a disk drive with no computing ability.
        </Typography>
        <Typography id={architecture.id} variant={'title'}>
          {architecture.display}
        </Typography>
        <img style={{maxWidth: 720}} src={HadoopArchitectureImage}/>
        <Typography>
          Here are the main components of Hadoop.
          <ul>
            <li><Bold>Namenode</Bold> - controls operation of the data jobs.</li>
            <li><Bold>Datanode</Bold> - this writes data in blocks to local storage. And it replicates data blocks to other datanodes. DataNodes are also rack-aware. You would not want to replicate all your data to the same rack of servers as an outage there would cause you to loose all your data.</li>
            <li><Bold>SecondaryNameNode</Bold> - this one take over if the primary Namenode goes offline.</li>
            <li><Bold>JobTracker</Bold> - sends MapReduce jobs to nodes in the cluster.</li>
            <li><Bold>TaskTracker</Bold> - accepts tasks from the Job Tracker.</li>
            <li><Bold>Yarn</Bold> - runs the Yarn components ResourceManager and NodeManager. This is a resource manager that can also run as a stand-alone component to provide other applications the ability to run in a distributed architecture. For example you can use Apache Spark with Yarn. You could also write your own program to use Yarn. But that is complicated.</li>
            <li><Bold>Client Application</Bold> - this is whatever program you have written or some other client like Apache Hive, Apache Spark, Apache Pig etc
              Application Master — runs shell commands in a container as directed by Yarn.</li>
          </ul>
        </Typography>
        <Typography id={storage.id} variant={'title'}>
          {storage.display}
        </Typography>
        <Typography>
          Hadoop can store its data in multiple file formats, mainly so that it can work with different cloud vendors and products. Here are some:
          <Code>
            {fileSystems}
          </Code>
          Hadoop also supports Windows Azure Storage Blobs (WASB), MapR, FTP, and others.
        </Typography>
        <Typography variant={'section'}>
          Writing to data files
        </Typography>
        <Typography>
          Hadoop is not a database. That means there is no random access to data and you cannot insert rows into tables or lines in the middle of files. Instead Hadoop can only write and delete files, although you can truncate and append to them, but that is not commonly done.
        </Typography>
        <Typography variant={'section'}>
          Hadoop as a platform for other products
        </Typography>
        <Typography>
          We are going to use Hadoop as a platform for products like Hive and Spark!
        </Typography>
      </Fragment>
    )
  }
}

export default withStyles(styles)(Hadoop)
