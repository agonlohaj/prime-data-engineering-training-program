/**
 * Created by Agon Lohaj on 20/07/2022.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import Typography from "presentations/Typography";
import React, {Fragment} from "react";
import {Bold, Italic} from 'presentations/Label'

import NYStockImage from 'assets/images/lecture2/stock_exchange.jpeg'
import FacebookImage from 'assets/images/lecture2/facebook.jpeg'
import AirplaneImage from 'assets/images/lecture2/big_jet.jpeg'
import Code from 'presentations/Code'

const json = `{
  name: "John",
  lastName: "Doe",
  age: 32,
  gender: "M"
}`

const styles = ({typography}) => ({
  root: {},
})

class Introduction extends React.Component {
  render() {
    const {classes, section} = this.props
    let whatIsBigData = section.children[0]
    let lifecycle = section.children[1]
    let ingesting = section.children[2]
    let persisting = section.children[3]
    let computeAnalise = section.children[4]
    let visualizing = section.children[5]
    let examples = section.children[6]
    return (<Fragment>
      <Typography variant={'heading'}>
        {section.display}
        <Divider/>
      </Typography>

      <Typography>
        Presenting some facts of mankind throughout the aeons:

        <ul>
          <li>
            Ancient humans stored information in cave paintings, the oldest we know of are over 40,000 years old. As
            humans evolved, the emergence of languages and the invention of writing led to detailed information being
            stored in various written forms, culminating with the invention of paper in China around the first century
            AD.
          </li>
          <li>
            The oldest printed books appeared in China between AD600 and AD900. For over a millennium, books remained
            the main source of information storage.
          </li>
          <li>
            Since the discovery of the transistor in 1947 and the integrated microchip in 1956, our society experienced
            a
            shift.
          </li>
          <li>
            The introduction of digital data storage also changed the way we produce, manipulate and store information.
            The transition point took place in 1996 when digital storage became more cost-effective for storing
            information than paper.
          </li>
        </ul>
      </Typography>

      <Typography>
        It all comes down to the information! Nowadays, digital information has become so entrenched in all aspects of
        our lives and society, that the recent growth in information production appears unstoppable.
      </Typography>
      <Typography>
        Each day on Earth we generate 500 million tweets, 294 billion emails, 4 million gigabytes of Facebook data,
        65 billion WhatsApp messages and 720,000 hours of new content added daily on YouTube.
      </Typography>
      <Typography>
        <Italic>Listening to the data is important... but so is experience and intuition. After all, what is intuition
          at its best but large amounts of data of all kinds filtered through a human brain rather than a math model?
        </Italic> - Steve Lohr
      </Typography>
      <Typography id={whatIsBigData.id} variant={'title'}>
        {whatIsBigData.display}
      </Typography>
      <Typography>
        Typically, when you think of a “computer,” you think about one machine sitting on your desk at
        home or at work. This machine works perfectly well for watching movies or working with
        spreadsheet software.
      </Typography>
      <Typography>
        However, as many users likely experience at some point, there are some
        things that your computer is not powerful enough to perform. One particularly challenging area
        is data processing. Single machines do not have enough power and resources to perform
        computations on huge amounts of information (or the user probably does not have the time to
        wait for the computation to finish).
      </Typography>
      <Typography>
        A cluster, or group, of computers, pools the resources of many machines together, giving us the ability to use
        all the cumulative resources as if they were a single computer. Now, a group of machines alone is not powerful,
        you need a framework to coordinate work across them. This is where Big Data and Big Data Technologies kick in!
      </Typography>
      <Typography>
        To be more accurate, big data clustering software combines the resources of many smaller machines, seeking to
        provide a number of benefits:
        <ul>
          <li>
            <Bold>Resource Pooling</Bold>: Combining the available storage space to hold data is a clear benefit, but
            CPU and memory pooling is also extremely important. Processing large datasets requires large amounts of
            all three of these resources.
          </li>
          <li>
            <Bold>High Availability</Bold>: Clusters can provide varying levels of fault tolerance and availability
            guarantees to prevent hardware or software failures from affecting access to data and processing. This
            becomes increasingly important as we continue to emphasize the importance of real-time analytics.
          </li>
          <li>
            <Bold>Easy Scalability</Bold>: Clusters make it easy to scale horizontally by adding additional machines
            to the group. This means the system can react to changes in resource requirements without expanding the
            physical resources on a machine.
          </li>
        </ul>
        <Italic>While Vertical Scaling is analog to a big strong wolf, horizontal scaling is analog to a Wolf
          Pack!</Italic>
      </Typography>

      <Typography id={lifecycle.id} variant={'title'}>
        {lifecycle.display}
      </Typography>
      <Typography>
        So how is data actually processed when dealing with a big data system? While approaches to implementation
        differ, there are some commonalities in the strategies and software that we can talk about generally. While
        the steps presented below might not be true in all cases, they are widely used.
      </Typography>
      <Typography>
        The general categories of activities involved with big data processing are:
        <ul>
          <li>Ingesting data into the system</li>
          <li>Persisting the data in storage</li>
          <li>Computing and Analyzing data</li>
          <li>Visualizing the results</li>
        </ul>
      </Typography>

      <Typography id={ingesting.id} variant={'title'}>
        {ingesting.display}
      </Typography>
      <Typography>
        Data ingestion is the process of taking raw data and adding it to the system. The complexity of this operation
        depends heavily on the format and quality of the data sources and how far the data is from the desired state
        prior to processing.
      </Typography>
      <Typography>
        During the ingestion process, some level of analysis, sorting, and labelling usually takes place. This process
        is sometimes called <Bold>ETL</Bold>, which stands for extract, transform, and load. While this term
        conventionally refers to legacy data warehousing processes, some of the same concepts apply to data entering
        the big data system. Typical operations might include modifying the incoming data to format it, categorizing
        and labelling data, filtering out unneeded or bad data, or potentially validating that it adheres to certain
        requirements.
      </Typography>
      <Typography>
        Technologies that can be used here are:
        <ul>
          <li><Bold>AWS Glue</Bold></li>
          <li><Bold>AWS Kinesis</Bold></li>
          <li><Bold>Apache Kafka</Bold> streaming data with a publish/subscribe orchestration</li>
          <li><Bold>Apache Sqoop</Bold> can take existing data from relational databases and add it to a big data
            system
          </li>
          <li><Bold>Apache Flume</Bold> and <Bold>Apache Chukwa</Bold> are projects designed to aggregate and import
            application and server logs.
          </li>
        </ul>
      </Typography>
      <Typography id={persisting.id} variant={'title'}>
        {persisting.display}
      </Typography>
      <Typography>
        The ingestion processes typically hand the data off to the components that manage storage, so that it can be
        reliably persisted to disk. While this seems like it would be a simple operation, the volume of incoming data,
        the requirements for availability, and the distributed computing layer make more complex storage systems
        necessary.
      </Typography>
      <Typography>
        This usually means leveraging a distributed file system for raw data storage. In this course we are going to
        focus on <Bold>Apache Hadoop's Hadoop Distributed File System (HDFS)</Bold> filesystem allow large quantities
        of data to be written across multiple nodes in the cluster. This ensures that the data can be accessed by
        compute resources, can be loaded into the cluster's RAM for in-memory operations, and can gracefully handle
        component failures. In combination with <Bold>Apache Parquet</Bold> as a columnar storage format which is
        available to any project in the Hadoop ecosystem, regardless of the choice of data processing framework, data
        model or programming language. Other distributed filesystems can be used in place of HDFS including <Bold>AWS
        Redshift</Bold>, as a columnar database.
      </Typography>
      <Typography id={computeAnalise.id} variant={'title'}>
        {computeAnalise.display}
      </Typography>
      <Typography>
        Once the data is available, the system can begin processing the data to surface actual information. The
        computation layer is perhaps the most diverse part of the system as the requirements and best approach can
        vary significantly depending on what type of insights desired. Data is often processed repeatedly, either
        iteratively by a single tool or by using a number of tools to surface different types of insights.
      </Typography>
      <Typography>
        <Bold>Batch processing</Bold> is one method of computing over a large dataset. The process involves breaking
        work up into smaller pieces, scheduling each piece on an individual machine, reshuffling the data based on the
        intermediate results, and then calculating and assembling the final result. These steps are often referred to
        individually as splitting, mapping, shuffling, reducing, and assembling, or collectively as a distributed map
        reduce algorithm. This is the strategy used by <Bold>Apache Hadoop's MapReduce</Bold>. Batch processing is
        most useful when dealing with very large datasets that require quite a bit of computation.
      </Typography>
      <Typography>
        While batch processing is a good fit for certain types of data and computation, other workloads require more
        real-time processing. Real-time processing demands that information be processed and made ready immediately
        and requires the system to react as new information becomes available. One way of achieving this is stream
        processing, which operates on a continuous stream of data composed of individual items. Another common
        characteristic of real-time processors is in-memory computing, which works with representations of the data in
        the cluster's memory to avoid having to write back to disk.
      </Typography>
      <Typography>
        <Bold>Apache Storm</Bold>, <Bold>Apache Flink</Bold>, and <Bold>Apache Spark</Bold> provide different ways of
        achieving real-time or near real-time processing. There are trade-offs with each of these technologies, which
        can affect which approach is best for any individual problem. In general, real-time processing is best suited
        for analyzing smaller chunks of data that are changing or being added to the system rapidly.
      </Typography>
      <Typography id={visualizing.id} variant={'title'}>
        {visualizing.display}
      </Typography>
      <Typography>
        Due to the type of information being processed in big data systems, recognizing trends or changes in data over
        time is often more important than the values themselves. Visualizing data is one of the most useful ways to
        spot trends and make sense of a large number of data points.
      </Typography>
      <Typography>
        This is where lots of tools and frameworks can be used, they are moslty referred as <Bold>Business
        Intelligence (BI)</Bold>, like <Bold>Prime</Bold>, <Bold>PowerBI</Bold>, <Bold>Prometheus</Bold>, <Bold>Elastic
        Stack</Bold>, <Bold>Elastic Search</Bold> etc.
      </Typography>
      <Typography id={examples.id} variant={'title'}>
        {examples.display}
      </Typography>
      <Typography>
        The <Bold>New York Stock Exchange</Bold> generates about one terabyte of new trade data per day.
      </Typography>
      <img src={NYStockImage}/>
      <Typography>
        The statistic shows that <Bold>500+ terabytes</Bold> of new data get ingested into the databases of social
        media site Facebook, every day. This data is mainly generated in terms of photo and video uploads, message
        exchanges, putting comments etc.
      </Typography>
      <img src={FacebookImage}/>
      <Typography>
        A single Jet engine can generate 10+terabytes of data in 30 minutes of flight time. With many thousand flights
        per day, generation of data reaches up to many Petabytes.
      </Typography>
      <img src={AirplaneImage}/>
    </Fragment>)
  }
}

export default withStyles(styles)(Introduction)
