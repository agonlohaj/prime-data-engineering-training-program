import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import { fade } from '@material-ui/core/styles/colorManipulator'
import withStyles from "@material-ui/core/styles/withStyles";
import classNames from "classnames";


const styles = ({palette, size, typography}) => ({
  wrapper: {
    position: 'absolute',
    zIndex: 300,
    width: '100%',
    height: '100%',
    background: fade('#fff', 0.7),
    top: 0,
    left: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  notInside: {
    position: 'fixed'
  },
  container: {
    textAlign: 'center',
    width: '100%',
    justifyContent: 'center',
    flexFlow: 'column wrap',
    alignItems: 'center',
    display: 'flex',
    left: 0
  },
  label: {
    margin: 0,
    // fontFamily: theme.fontFamily,
    color: palette.disabledColor,
    fontSize: size.fontSize
  },
  buttonLabel: {
    color: palette.textColor,
    textTransform: 'uppercase',
    fontWeight: 'normal',
    fontSize: size.fontSize
  },
  labelWrapper: {
    textAlign: 'center'
  },
  info: {
    marginTop: 0,
    marginBottom: size.spacing / 2,
    cursor: 'text',
    fontSize: size.titleFontSize
  }
})

/**
 * Renders an error container with retry option
 */
export class ErrorBox extends React.Component {
  /**
   * propTypes
   * @type {object}
   * @property {string} message - error message
   * @property {function} onRetryClicked - handler
   * @property {object} theme - redux store theme
   * @property {object} size - redux store sizing
   * @property {object} strings - related strings based on locale
   */
  static get propTypes () {
    return {
      // provided by parent
      message: PropTypes.string,
      onRetryClicked: PropTypes.func
    }
  }

  preventDefault (event) {
    event.stopPropagation()
  }

  renderError () {
    let {message = '', errorContainerStyle = {}, classes, onRetryClicked} = this.props

    let paragraphProps = {
      onMouseDown: this.preventDefault.bind(this),
      onMouseMove: this.preventDefault.bind(this),
      onMouseUp: this.preventDefault.bind(this)
    }

    let nestedMessage = message
    if (message && message.message) {
      nestedMessage = JSON.stringify(message.message)
    }
    return (
      <div
        className={classes.container}
        style={{
          ...errorContainerStyle,
          display: !nestedMessage && 'none'
        }}>
        <p {...paragraphProps} className={classes.label}>
          {message}
        </p>
        {onRetryClicked && <Button
          onClick={ (e) => {
            e.preventDefault()
            e.stopPropagation()
            onRetryClicked()
          }}>
          <div className={classes.labelWrapper}>
            <label className={classes.buttonLabel}>
              Retry
            </label>
          </div>
        </Button>}
      </div>
    )
  }

  /**
   * renders a loading indicator
   * @return {HTML} markup
   */
  render () {
    let {message, isInside = true, className, classes, style = {}} = this.props
    return (
      <div style={{display: !message && 'none', ...style}} className={classNames(classes.wrapper, !isInside && classes.notInside, className)}>
        {this.renderError()}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({

})

export default withStyles(styles)(connect(mapStateToProps)(ErrorBox))
