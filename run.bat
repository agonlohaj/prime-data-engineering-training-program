:: Start MongoDB
start cmd.exe /k "mongod --replSet development --port 27017 --bind_ip 127.0.0.1 --dbpath /usr/local/var/mongodb  --oplogSize 128"

:: cd front end directory, and start it
cd frontend
start cmd.exe /k "npm start"

:: cd to api server, and start that
cd ../backend
start cmd.exe /k "sbt run"