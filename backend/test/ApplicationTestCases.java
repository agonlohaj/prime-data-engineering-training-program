import org.junit.Test;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.route;

public class ApplicationTestCases extends WithApplication {

	@Test
	public void testIndex() {
		final Http.RequestBuilder homeRequest = new Http.RequestBuilder().method("GET").uri("/");
		final Result result = route(app, homeRequest);
		assertEquals(OK, result.status());
		assertEquals("text/html", result.contentType().get());
		assertEquals("utf-8", result.charset().get());
		assertTrue(contentAsString(result).contains("Welcome to your Play Web Application!"));
	}
}
