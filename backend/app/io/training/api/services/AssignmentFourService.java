package io.training.api.services;

import algebra.lattice.Bool;
import com.google.inject.Inject;
import io.training.api.executors.SparkExecutionContext;
import io.training.api.models.requests.BinarySearchRequest;
import io.training.api.models.requests.NameValuePair;
import io.training.api.models.responses.ChartData;
import io.training.api.spark.ISparkSessionProvider;
import io.training.api.utils.DatabaseUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.apache.spark.sql.functions.col;

/**
 * Created by Agon on 09/08/2020
 */
@Singleton
public class AssignmentFourService {
    @Inject
    SparkExecutionContext sparkExecutionContext;
    @Inject
    ISparkSessionProvider provider;
    @Inject
    HttpExecutionContext ec;

    public CompletableFuture<Dataset<Row>> sqlPlayground() {
        return CompletableFuture.supplyAsync(() -> provider.getSparkSession().sql(getSQLPlayground()), sparkExecutionContext);
    }

    public CompletableFuture<Dataset<Row>> assignment1() {
        return CompletableFuture.supplyAsync(() -> provider.getSparkSession().sql(getAssignment1SQL()), sparkExecutionContext);
    }

    public CompletableFuture<Dataset<Row>> assignment2() {
        return CompletableFuture.supplyAsync(() -> provider.getSparkSession().sql(getAssignment2SQL()), sparkExecutionContext);
    }


    public CompletableFuture<Dataset<Row>> assignment3() {
        return CompletableFuture.supplyAsync(() -> provider.getSparkSession().sql(getAssignment3SQL()), sparkExecutionContext);
    }


    public CompletableFuture<Dataset<Row>> assignment4() {
        return CompletableFuture.supplyAsync(() -> provider.getSparkSession().sql(getAssignment4SQL()), sparkExecutionContext);
    }


    public CompletableFuture<Dataset<Row>> assignment5() {
        return CompletableFuture.supplyAsync(() -> provider.getSparkSession().sql(getAssignment5SQL()), sparkExecutionContext);
    }

    /**
     * In the following assignments, you will have to use these tables (same as previous):
     *
     * customers        ->  `CustomerID` STRING,    `CustomerName` STRING,  `ContactName` STRING,   `Address` STRING,
     *                      `City` STRING,          `PostalCode` STRING,    `Country` STRING
     *
     * employees        ->  `EmployeeID` STRING,    `LastName` STRING,      `FirstName` STRING,     `BirthDate` STRING,
     *                      `Photo` STRING,         `Notes` STRING
     *
     * orders           ->  `OrderID` STRING,       `CustomerID` STRING,    `EmployeeID` STRING,    `OrderDate` STRING,
     *                      `ShipperID` STRING
     *
     * order_details    ->  `OrderDetailID` STRING, `OrderID` STRING,       `ProductID` STRING,     `Quantity` STRING
     *
     * products         ->  `ProductID` STRING,     `ProductName` STRING,   `SupplierID` STRING,    `CategoryID` STRING,
     *                      `Unit` STRING,`Price` STRING
     *
     * You can query the table through the name, as: `select * from employees*`
     */

    /**
     * Playground for training purposes
     */
    private String getSQLPlayground() {
        return "select * from orders left join order_details on orders.OrderID = order_details.OrderID limit 2";
    }

    /**
     * Assignment 1: Find the product (`ProductID` and `ProductName`) that has been sold the most
     */
    private String getAssignment1SQL() {
        return null;
    }

    /**
     * Assignment 2: Find the categories that have sales of over 50.000, excluding sales supplier `SupplierId` = 1
     */
    private String getAssignment2SQL() {
        return null;
    }

    /**
     * Assignment 3: Find the top 10 customers (`CustomerName`, `Address` and `City`) who have ordered the most
     */
    private String getAssignment3SQL() {
        return null;
    }

    /**
     * Assignment 4: Find the customers (`CustomerName`, `ContactName`, `NrOfDays`) that have ordered the most in consecutive days
     */
    private String getAssignment4SQL() {
        return null;
    }

    /**
     * Assignment 5: Find all the days that the customer did not do an order from the first date to the last that he purchased (`CustomerID` and `days`).
     */
    private String getAssignment5SQL() {
        return null;
    }

    /**
     * Using Backtracking algorithm I am going to solve the Knights Tour Problem!
     *
     * @return whether a solution is possible or not
     */
    public CompletableFuture<Boolean> knightTour() {
        // TODO: Implement this function
        return CompletableFuture.supplyAsync(() -> {
            return false;
        }, ec.current());
    }
}
