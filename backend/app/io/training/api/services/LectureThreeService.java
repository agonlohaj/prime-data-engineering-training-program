package io.training.api.services;

import com.google.inject.Inject;
import io.training.api.executors.SparkExecutionContext;
import io.training.api.spark.ISparkSessionProvider;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import javax.inject.Singleton;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.apache.spark.sql.functions.*;

/**
 * Created by Agon on 09/08/2020
 */
@Singleton
public class LectureThreeService {
    @Inject
    SparkExecutionContext sparkExecutionContext;
    @Inject
    ISparkSessionProvider provider;

    /**
     * Exercise 1: Create a Spark program to read the airport data from data/s3.spark.rdd.api/airport/airports.txt, find all the
     * airports which are located in the United States.
     *
     * @return
     */
    public CompletableFuture<List<String>> exercise1() {
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            JavaSparkContext sc = new JavaSparkContext(session.sparkContext());

            // Same as the example on the lecture
            return sc
                    .textFile("data/s3.spark.rdd.api/airport/airports.txt")
                    .filter(line -> line.split(",")[3].equals("United States"))
                    .collect();
        }, sparkExecutionContext);
    }

    /**
     * Exercise 2: Create two RDDs and apply union, intersection, and subtract operations.
     *
     * @return
     */
    public CompletableFuture<List<String>> exercise2() {
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            JavaSparkContext sc = new JavaSparkContext(session.sparkContext());

            // Creating two RDD tables with simple integers
            JavaRDD<Integer> first = sc.parallelize(Arrays.asList(1, 2, 3, 4, 5));
            JavaRDD<Integer> second = sc.parallelize(Arrays.asList(4, 5, 6));

            // The union, intersection and subtract operators
            List<Integer> union = first.union(second).collect();
            List<Integer> intersection = first.intersection(second).collect();
            List<Integer> subtract = first.subtract(second).collect();

            Function<List<Integer>, String> mapping =
                    (List<Integer> t) -> t.stream().map(String::valueOf).collect(Collectors.joining(","));

            // Constructing an array to output our results
            List<String> results = new ArrayList<>();
            results.add("First dataset: [1, 2, 3, 4, 5]");
            results.add("Second dataset: [4, 5, 6]");

            results.add("--------------------------------");

            results.add("Union: [" + mapping.apply(union) + "]");
            results.add("Intersection: [" + mapping.apply(intersection) + "]");
            results.add("Subtract: [" + mapping.apply(subtract) + "]");

            return results;
        }, sparkExecutionContext);
    }

    /**
     * Exercise 3: Create a Spark program to read the first 100 prime numbers from data/s3.spark.rdd.api/prime_nums/prime_nums.txt,
     * print the sum of those numbers to console. Each row of the input file contains 10 prime numbers separated by spaces.
     *
     * @return
     */
    public CompletableFuture<Integer> exercise3() {
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();
            JavaSparkContext sc = new JavaSparkContext(session.sparkContext());

            // Reading the text file as an RDD
            JavaRDD<String> primeNumsDS = sc.textFile("data/s3.spark.rdd.api/prime_nums/prime_nums.txt");

            // Splitting the lines on whitespaces, then flat mapping
            JavaRDD<String> numbers = primeNumsDS.flatMap(line -> Arrays.asList(line.split("\\s+")).iterator());

            JavaRDD<String> validNumbers = numbers.filter(number -> !number.isEmpty());
            JavaRDD<Integer> intNumbers = validNumbers.map(Integer::valueOf);

            return intNumbers.reduce(Integer::sum);
        }, sparkExecutionContext);
    }

    /**
     * Exercise 4: Read the words.txt file using sc.textFile("data/s3.spark.rdd.api/words/words.txt"); and count how many times a word repeats.
     * The implementation of this exercise will be done at LectureThreeService the method exercise4
     *
     * @return
     */
    public CompletableFuture<Map<String, Long>> exercise4() {
        return CompletableFuture.supplyAsync(() -> {
            try {
                SparkSession session = provider.getSparkSession();
                JavaSparkContext sc = new JavaSparkContext(session.sparkContext());

                // Same as the example above
                JavaRDD<String> lines = sc.textFile("data/s3.spark.rdd.api/words/words.txt");
                JavaRDD<String> words = lines.flatMap(line -> Arrays.asList(line.split(" ")).iterator());

                return words.countByValue();
            } catch (Exception ex) {
                ex.printStackTrace();
                throw new CompletionException(ex);
            }
        }, sparkExecutionContext);
    }

    /**
     * Exercise 5: Read from employees and print the schema, find the employee with the highest salary, also find the total salary given to all employees.
     * Show the results in the console.
     *
     * @return
     */
    public CompletableFuture<List<String>> exercise5() {
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();

            // Reading as a dataset
            Dataset<Row> ds = session.read().json("data/s3.spark.dataset_api/employees/employees.json");

            // Extracting schema from the dataset (automatically inferred)
            String schema = ds.schema().toDDL();

            // Aggregating to find the maximum and the total
            Dataset<Row> highestSalary = ds.agg(max("salary").as("max_salary"));
            Dataset<Row> totalSalary = ds.agg(sum("salary").as("total_salary"));

            // Preparing results to show to the page
            List<String> result = new ArrayList<>();
            result.add("Schema: " + schema);
            result.add("Highest salary: " + highestSalary.first().get(0));
            result.add("Total salary: " + totalSalary.first().get(0));

            return result;
        }, sparkExecutionContext);
    }

    /**
     * Create a Dataset by reading the file students.csv, using SQL statement, filter by subject 'Modern Art' and year 2007.
     * Limit the response to 20 objects only.
     *
     * @return
     */
    public CompletableFuture<Dataset<Row>> exercise6() {
        return CompletableFuture.supplyAsync(() -> {
            SparkSession session = provider.getSparkSession();

            Dataset<Row> ds = session.read()
                    .option("header", true)
                    .csv("data/s3.spark.sql/students/students.csv");

            // Creation of temporary view
            ds.createOrReplaceTempView("ds");

            // Simple filtering on Modern Art and 2007 data
            return session.sql("select * from ds where subject = 'Modern Art' and year=2007").limit(20);
        }, sparkExecutionContext);
    }
}
