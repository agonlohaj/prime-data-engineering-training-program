package io.training.api.modules;

import akka.actor.ActorSystem;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.typesafe.config.Config;
import play.Logger;

@Singleton
public class ApplicationStartProvider {

    @Inject
    public ApplicationStartProvider(Config config, ActorSystem system) {
        String mode = config.getString("mode");
        Logger.of(this.getClass()).debug("------- Running in mode -------");
        Logger.of(this.getClass()).debug(mode);
        // Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();
        Logger.of(this.getClass()).debug("------- Heap utilization statistics [MB] -------");
        int mb = 1024 * 1024;
        //Print total available memory
        Logger.of(this.getClass()).debug("Total Memory:" + runtime.totalMemory() / mb);
        //Print Maximum available memory
        Logger.of(this.getClass()).debug("Max Memory:" + runtime.maxMemory() / mb);

        String secret = config.getString("play.http.secret.key");
        Logger.of(this.getClass()).debug("Play SECRET, {}", secret);
        Logger.of(this.getClass()).debug("SYSTEM NAME: {}", system.name());
        // Akka Management hosts the HTTP routes used by bootstrap
    }
}