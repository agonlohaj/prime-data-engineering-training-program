package io.training.api.modules;

import com.google.inject.AbstractModule;
import com.typesafe.config.Config;
import io.training.api.spark.ISparkSessionProvider;
import io.training.api.spark.StandaloneSparkSessionProvider;
import play.Environment;

public class SparkModule extends AbstractModule {
    private final Config config;

    public SparkModule(Environment environment, Config config) {
      this.config = config;
    }  

    @Override
    protected void configure() {
		String mode = config.getString("spark.mode");
		System.out.println("Running on spark mode: " + mode);
		bind(ISparkSessionProvider.class).to(StandaloneSparkSessionProvider.class).asEagerSingleton();
    }
}