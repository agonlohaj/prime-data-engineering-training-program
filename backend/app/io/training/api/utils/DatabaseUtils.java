package io.training.api.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.training.api.exceptions.RequestException;
import org.apache.parquet.Strings;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.bson.Document;
import play.i18n.Messages;
import play.i18n.MessagesApi;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionException;
import java.util.stream.Collectors;

import static play.mvc.Results.badRequest;
import static play.mvc.Results.status;

/**
 * Helper class that formats data Created by Agon on 10/13/2016.
 */
public class DatabaseUtils {


	public static Result throwableToResult(Throwable throwable) {
		try {
			Result result = DatabaseUtils.statusFromThrowable(throwable);
			if (result != null) {
				return result;
			}
			return badRequest(DatabaseUtils.objectNodeFromError(throwable.getMessage()));
		} catch (Exception ex) {
			ex.printStackTrace();
			return badRequest(DatabaseUtils.objectNodeFromError(throwable.getMessage()));
		}
	}


	public static Result statusFromThrowable(Throwable error) {
		if (error instanceof RequestException) {
			return DatabaseUtils.statusFromThrowable((RequestException) error);
		}
		if (error.getCause() == null) {
			return null;
		}
		return DatabaseUtils.statusFromThrowable(error.getCause());
	}

	private static Result statusFromThrowable(RequestException requestException) {
		ObjectNode error = DatabaseUtils.objectNodeFromError(requestException.getMessage());
		if (requestException.getCause() != null) {
			error.put("cause", requestException.getCause().getMessage());
		}
		return status(requestException.getStatusCode(), error);
	}

	private static ObjectNode objectNodeFromError(String error) {
		return DatabaseUtils.objectNodeFromError(Json.toJson(error));
	}

	private static ObjectNode objectNodeFromError(JsonNode error) {
		ObjectNode result = Json.newObject();
		result.put("status", false);
		result.put("message", error);
		return result;
	}


	public static <T> List<T> parseJsonListOfType (JsonNode json, Class<T> type) {
		try {
			if (!json.isArray()) {
				throw new RequestException(Http.Status.BAD_REQUEST, "invalid_parameters");
			}
			List<T> list = new ArrayList<>();
			for (JsonNode node: json) {
				list.add(Json.fromJson(node, type));
			}
			return list;
		} catch (RequestException | ClassCastException ex) {
			ex.printStackTrace();
			throw new CompletionException(ex);
		} catch (CompletionException ex) {
			ex.printStackTrace();
			throw ex;
		} catch (Exception e) {
			e.printStackTrace();
			throw new CompletionException(new RequestException(Http.Status.INTERNAL_SERVER_ERROR, "service_unavailable"));
		}
	}


	/**
	 * Converts a Document to a JSON node
	 * @param item
	 * @return
	 */
	public static JsonNode toJson (Document item) {
		return Json.parse(item.toJson());
	}


	/**
	 * Converts a Document to a JSON node
	 * @param items
	 * @return
	 */
	public static JsonNode toJson (List<Document> items) {
		ArrayNode array = Json.newArray();
		for (Document item: items) {
			array.add(DatabaseUtils.toJson(item));
		}
		return array;
	}

	/**
	 * Converts a Dataset<Row> response to Json Node
	 * @param items
	 * @return
	 */
	public static <T> JsonNode toJson (Dataset<T> items) {
		List<String> response = items.toJSON().collectAsList();
		return Json.parse(String.format("[%s]", Strings.join(response, ",")));
	}

	/**
	 * parses a JSON object node and converts it to a mongodb java driver
	 * Document
	 * @param value
	 * @return
	 */
	public static Document toDocument (ObjectNode value) {
		return Document.parse(value.toString());
	}


	/**
	 * Parses an array node to a list of documents
	 * @param json
	 * @return
	 */
	public static List<Document> toListDocument(ArrayNode json) {
		List<Document> result = new ArrayList<>();
		for (JsonNode node : json) {
			result.add(toDocument((ObjectNode) node));
		}
		return result;
	}
}
