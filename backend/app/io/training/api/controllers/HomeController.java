package io.training.api.controllers;

import com.google.inject.Inject;
import com.typesafe.config.Config;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.concurrent.CompletableFuture;


@Api(value = "Home Controller")
public class HomeController extends Controller {
	@Inject
	Config config;
	@Inject
	HttpExecutionContext ec;

	@ApiOperation(value = "Home Page")
	public CompletableFuture<Result> index() {
		return CompletableFuture.supplyAsync(() -> ok(views.html.index.render(config)), ec.current());
	}
}